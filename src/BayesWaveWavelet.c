/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg, Margaret Millhouse
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "BayesWave.h"
#include "BayesWaveMath.h"
#include "BayesWaveWavelet.h"


/* ********************************************************************************** */
/*                                                                                    */
/*                            Wavelet basis functions                                 */
/*                                                                                    */
/* ********************************************************************************** */

double SineGaussianSNR(double *params, double *Snf, double Tobs)
{
   int i = (int)(params[1]*Tobs);

   return params[3]*sqrt((params[2]/(2.0*RT2PI*params[1]))/(Snf[i]*2.0/Tobs));
}


void SineGaussianBandwidth(double *sigpar, double *fmin, double *fmax)
{

   double f0 = sigpar[1];
   double Q  = sigpar[2];

   double tau = Q/(LAL_TWOPI*f0);

   *fmax = f0 + 3.0/(tau);  // no point evaluating waveform past this time (many efolds down)
   *fmin = f0 - 3.0/(tau);  // no point evaluating waveform before this time (many efolds down)
   
}

void SineGaussianFourier(double *hs, double *sigpar, int N, int flag, double Tobs)
{
   double f0, t0, Q, sf, sx, Amp;
   double fmax, fmin;//, fac;
   double phi, f;
   double tau;
   double re,im;
   double invTobs = 1.0/Tobs;

   int i,istart,istop,imin,imax,iend,even,odd;

   t0  = sigpar[0];
   f0  = sigpar[1];
   Q   = sigpar[2];
   Amp = sigpar[3];
   phi = sigpar[4];

   SineGaussianBandwidth(sigpar,&fmin,&fmax);

   tau = Q/(LAL_TWOPI*f0);

   //Make sure frequency is in range
   if(fmin < 0.0)
      fmin=0.0;
   if(fmax > (double)(N/2-1)*invTobs)
      fmax = (double)(N/2-1)*invTobs;

   imin = (int)(fmin*Tobs);
   imax = (int)(fmax*Tobs);
   iend = imax+1;

   //fac = sqrt(Tobs);

   if(flag==0)
   {
      istart = 0;
      istop  = N/2;
   }
   else
   {
      istart = imin;
      istop  = imax;
   }

   /* Use recursion relationship for cos(Phase) & sin(Phase) */

   //initial values of exp(iPhase)
   double phase = LAL_TWOPI*fmin*t0;
   double cosPhase_m  = cos(phase-phi);
   double sinPhase_m  = sin(phase-phi);
   double cosPhase_p  = cos(phase+phi);
   double sinPhase_p  = sin(phase+phi);

   //incremental values of exp(iPhase)
   double dim = -sin(LAL_TWOPI*t0*invTobs);
   double dre = sin(0.5*(LAL_TWOPI*t0*invTobs));
   dre = -2.0*dre*dre;

   double amplitude = 0.5*(Amp)*RTPI*tau;
   double pi2tau2   = LAL_PI*LAL_PI*tau*tau;
   double Q2        = Q*Q/f0;

    if(flag==0)
    {
        for(i = istart; i < imin; i++)
        {
            even = 2*i;
            odd = even + 1;

            hs[even] = hs[odd] = 0.0;
        }

        for(i = iend; i < istop; i++)
        {
            even = 2*i;
            odd = even + 1;

            hs[even] = hs[odd] = 0.0;
        }

    }

   for(i = imin; i < iend; i++)
   {
      even = 2*i;
      odd = even + 1;
      f = (double)(i)*invTobs;

      if(flag==0) hs[even] = hs[odd] = 0.0;

      sf = amplitude*expf(-pi2tau2*(f-f0)*(f-f0));
      sx = expf(-Q2*f);
      re = sf*(cosPhase_m+sx*cosPhase_p);
      im = sf*(sinPhase_m+sx*sinPhase_p);

      switch(flag)
      {
         case 1: // Add new wavelet to hs[]
            hs[even] += re;
            hs[odd]  += im;
            break;
         case -1: // Remove new wavelet from hs[]
            hs[even] -= re;
            hs[odd]  -= im;
            break;
         case 0:  // Replace hs[] with new wavelet
            hs[even] = re;
            hs[odd]  = im;
            break;
         default:
            break;
      }

      /* Now update re and im for the next iteration. */
      recursive_phase_evolution(dre, dim, &cosPhase_m, &sinPhase_m);
      recursive_phase_evolution(dre, dim, &cosPhase_p, &sinPhase_p);
   }
}

void SineGaussianFisher(double *params, struct FisherMatrix *fisher)
{
  int ii, jj;

  double f0  = params[1];
  double Q   = params[2];

  //precomput repeated constants
  double PI2  = LAL_PI*LAL_PI;
  double f2   = f0*f0;
  double Q2   = Q*Q;
  double invf = 1./f0;
  double invQ = 1./Q;

  // Zero matrix out
  for (ii = 0; ii < 5; ii++) {
    for (jj = 0; jj < 5; jj++) {
      fisher->matrix[ii][jj] = 0.0;
    }
  }

  // only calculate non-zero componenets

  // t t
  fisher->matrix[0][0] = 4.0*f2*PI2*invQ*invQ;

  // t phi
  fisher->matrix[0][4] = (-2.0*f0*LAL_PI);
  fisher->matrix[4][0] = fisher->matrix[0][4];

  // f f
  fisher->matrix[1][1] = 0.25*(3.0+Q2)*invf*invf;

  // f Q
  fisher->matrix[1][2] = 0.25*(-3.0)*invf*invQ;
  fisher->matrix[2][1] = fisher->matrix[1][2];

  // f lnA
  fisher->matrix[1][3] = -0.5*invf;
  fisher->matrix[3][1] = fisher->matrix[1][3];

  // Q Q
  fisher->matrix[2][2] = 0.25*3.0*invQ*invQ;

  // Q lnA
  fisher->matrix[2][3] = 0.5*invQ;
  fisher->matrix[3][2] = fisher->matrix[2][3];

  // lnA lnA
  fisher->matrix[3][3] = 1.0;

  // phi phi
  fisher->matrix[4][4] = 1.0;

  // Get eigenvectors/values for Fisher matrix
  matrix_eigenstuff(fisher->matrix, fisher->evector, fisher->evalue, fisher->N);


}

/////////////////////////////////////////////////////////
//        "Chirplets"
/////////////////////////////////////////////////////////

double ChirpletSNR(double *params, double *Snf, double Tobs)
{
    int i = (int)(params[1]*Tobs);
    
    return params[3]*sqrt((params[2]/(2.0*RT2PI*params[1]))/(Snf[i]*2.0/Tobs));
}


void ChirpletBandwidth(double *sigpar, double *fmin, double *fmax)
{
    
    // Same as SG bandwidth... make a separate subroutine for consistency
    
    double f0 = sigpar[1];
    double Q  = sigpar[2];
    
    double tau = Q/(LAL_TWOPI*f0);
    
    *fmax = f0 + 3.0/(tau);  // no point evaluating waveform past this time (many efolds down)
    *fmin = f0 - 3.0/(tau);  // no point evaluating waveform before this time (many efolds down)
    
}

/* void ChirpGaussianF(double *hs, double *sigpar, double TOBS, int NMAX) */
void ChirpletFourier(double *hs, double *sigpar, int N, int flag, double Tobs)
{
    double f0, t0, Q, sf, sx, Amp, beta;
    double fmax, fmin, fac;
    double phi, f;
	double dn, trm, phz;
    double tau, delta,re, im;
	int imin, imax, istart, istop, iend, even, odd;
    double invTobs = 1.0/Tobs;
    
    int i;
	
    t0 = sigpar[0];
    f0 = sigpar[1];
    Q = sigpar[2];
    Amp = sigpar[3];
    phi = sigpar[4];
	beta = sigpar[5];
    
	ChirpletBandwidth(sigpar,&fmin,&fmax);
    
    tau = Q/(LAL_TWOPI*f0);
    
    //Make sure frequency is in range
    if(fmin < 0.0)
    fmin=0.0;
    if(fmax > (double)(N/2-1)*invTobs)
    fmax = (double)(N/2-1)*invTobs;
    
    imin = (int)(fmin*Tobs);
    imax = (int)(fmax*Tobs);
    iend = imax+1;
    
    if(flag==0)
    {
        istart = 0;
        istop  = N/2;
    }
    else
    {
        istart = imin;
        istop  = imax;
    }
	
    // Some definitions
	dn = (1.0+LAL_PI*LAL_PI*beta*beta);
	delta = 0.5*atan(LAL_PI*beta);
	trm = LAL_PI*LAL_PI*LAL_PI*beta*tau*tau/dn;
    fac = sqrt(Tobs);
    
    // Different for adding, subtracting, nothinging a wavelet
    
    
    if(flag==0)
    {
        for(i = istart; i < imin; i++)
        {
            even = 2*i;
            odd = even + 1;
            
            hs[even] = hs[odd] = 0.0;
        }
        
        for(i = iend; i < istop; i++)
        {
            even = 2*i;
            odd = even + 1;
            
            hs[even] = hs[odd] = 0.0;
        }
        
    }
    
    for(i = imin; i < iend; i++)
    {
        even = 2*i;
        odd = even + 1;
        f = (double)(i)*invTobs;
        
        if(flag==0) hs[even] = hs[odd] = 0.0;
        
        sf = (Amp/fac)*RTPI/2.0*tau*exp(-LAL_PI*LAL_PI*tau*tau*(f-f0)*(f-f0)/dn);
        sx = exp(-Q*Q*f/f0);
        phz = phi+delta-(f-f0)*(f-f0)*trm;
        re = sf*(cos(LAL_TWOPI*f*t0-phz)+sx*cos(LAL_TWOPI*f*t0+phz));
        im = sf*(sin(LAL_TWOPI*f*t0-phz)+sx*sin(LAL_TWOPI*f*t0+phz));
        
        //printf("%g %g\n",re,im);
        
        switch(flag)
        {
            case 1: // Add new wavelet to hs[]
                hs[even] += re;
                hs[odd]  += im;
                break;
            case -1: // Remove new wavelet from hs[]
                hs[even] -= re;
                hs[odd]  -= im;
                break;
            case 0:  // Replace hs[] with new wavelet
                hs[even] = re;
                hs[odd]  = im;
                break;
            default:
                break;
        }
        
        /* Now update re and im for the next iteration. */
//        recursive_phase_evolution(dre, dim, &cosPhase_m, &sinPhase_m);
//        recursive_phase_evolution(dre, dim, &cosPhase_p, &sinPhase_p);
    }

    
    /*
    for(i = 0; i < NMAX/2; i++)
    {
		hs[2*i] += 0.0;
		hs[2*i+1] += 0.0;
		
		if(i > imin && i < imax)
		{
			f = (double)(i)/Tobs;
			sf = (Amp/fac)*RTPI/2.0*tau*exp(-PI*PI*tau*tau*(f-f0)*(f-f0)/dn);
            
			sf /= dnx;
			sx = exp(-Q*Q*f/f0);
			phz = phi+delta-(f-f0)*(f-f0)*trm;
			hs[2*i] += sf*(cos(TPI*f*t0-phz)+sx*cos(TPI*f*t0+phz));
			hs[2*i+1] += sf*(sin(TPI*f*t0-phz)+sx*sin(TPI*f*t0+phz));
            
            
		}
		
    }
     */
    
}

void ChirpletFisher(double *params, struct FisherMatrix *fisher)
{
    double f, Q, beta;
    int ii, jj, Nparams;
    
    Nparams = 6;
    
    f = params[1];
    Q = params[2];
    beta = params[5];
    
    
    // Zero matrix out
    for (ii = 0; ii < Nparams; ii++) {
        for (jj = 0; jj < Nparams; jj++) {
            fisher->matrix[ii][jj] = 0.0;
        }
    }
    
    // t t
    fisher->matrix[0][0] = (4.0*f*f*LAL_PI*LAL_PI*(1.+Q*Q+LAL_PI*LAL_PI*beta*beta))/(Q*Q);
    
    // t f
    fisher->matrix[0][1] = (-2.0*LAL_PI*LAL_PI*beta);
    fisher->matrix[1][0] = fisher->matrix[0][1];
    
    // t Q
    fisher->matrix[0][2] = LAL_PI*LAL_PI*beta*f/Q;
    fisher->matrix[2][0] = fisher->matrix[0][2];
    
    // t phi
    fisher->matrix[0][4] = (-2.0*f*LAL_PI);
    fisher->matrix[4][0] = fisher->matrix[0][4];
    
    // t beta
    fisher->matrix[0][5] = (-LAL_PI*LAL_PI*f)/2.0;
    fisher->matrix[5][0] = fisher->matrix[0][5];
    
    // f f
    fisher->matrix[1][1] = ((3.0+Q*Q+3.0*LAL_PI*LAL_PI*beta*beta)/(4.0*f*f));
    
    // f Q
    fisher->matrix[1][2] = (-3.0*(1.0+LAL_PI*LAL_PI*beta*beta)/(4.0*f*Q));
    fisher->matrix[2][1] = fisher->matrix[1][2];
    
    // f lnA
    fisher->matrix[1][3] = -(1.0/(2.0*f));
    fisher->matrix[3][1] = fisher->matrix[1][3];
    
    // f phi
    fisher->matrix[1][4] = LAL_PI*beta/(2.0*f);
    fisher->matrix[4][1] = fisher->matrix[1][4];
    
    // f beta
    fisher->matrix[1][5] = 3.0*beta*LAL_PI*LAL_PI/(8.0*f);
    fisher->matrix[5][1] = fisher->matrix[1][5];
    
    // Q Q
    fisher->matrix[2][2] = (3.0*(1.0+LAL_PI*LAL_PI*beta*beta)/(4.0*Q*Q));
    
    // Q lnA
    fisher->matrix[2][3] = (1.0/(2.0*Q));
    fisher->matrix[3][2] = fisher->matrix[2][3];
    
    // Q phi
    fisher->matrix[2][4] = -(LAL_PI*beta)/(2.0*Q);
    fisher->matrix[4][2] = fisher->matrix[2][4];
    
    // Q beta
    fisher->matrix[2][5] = -(3.0*beta*LAL_PI*LAL_PI)/(8.0*Q);
    fisher->matrix[5][2] = fisher->matrix[2][5];
    
    // lnA lnA
    fisher->matrix[3][3] = 1.0;
    
    // phi phi
    fisher->matrix[4][4] = 1.0;
    
    // phi beta
    fisher->matrix[4][5] = LAL_PI/4.0;
    fisher->matrix[5][4] = fisher->matrix[4][5];
    
    // beta beta
    fisher->matrix[5][5] = 3.0*LAL_PI*LAL_PI/16.0;
    
    
    // Get eigenvectors/values for Fisher matrix
    matrix_eigenstuff(fisher->matrix, fisher->evector, fisher->evalue, fisher->N);
    
    
}

/* ********************************************************************************** */
/*                                                                                    */
/*                         Network projection functions                               */
/*                                                                                    */
/* ********************************************************************************** */


void computeProjectionCoeffs(struct Data *data, struct Network *projection, double *extParams, double fmin, double fmax)
{
   int i,re,im;
  //TODO: Why can't I start projection at fmin
  fmin=0.0;
   // get needed constants from data structure
   int N  = data->N;
   int NI = data->NI;
   double Tobs = data->Tobs;

  int ifo;
  int istart = (int)(fmin*Tobs);
    int iend   = (int)(fmax*Tobs);
    if(istart<0) istart = 0;
    if(iend>N/2) iend   = N/2;

   double df,freq;
   double Fplus, Fcross;

   // common extrinsic parameters
   //[0] alpha, [1] sin(delta) [2] psi [3] ellipticity [4] phi0 [5] scale

   double alpha    = extParams[0];
   double sindelta = extParams[1];
   double psi      = extParams[2];
   double phi0     = extParams[4];

   double cosPhase;
   double sinPhase;

   double dim,dre;

    double toff = 0.0;
   double trigArgument=0.0;

   for(ifo = 0; ifo<NI; ifo++)
   {
      //Get time offsets for detectors
      projection->dtimes[ifo] = XLALTimeDelayFromEarthCenter(data->detector[ifo]->location, alpha, asin(sindelta), &data->epoch);

      //Calculate antenna pattern once for each detector
      XLALComputeDetAMResponse(&Fplus, &Fcross, (const REAL4(*)[3])data->detector[ifo]->response, alpha, asin(sindelta), psi, XLALGreenwichMeanSiderealTime(&data->epoch));

      //initialize times
      toff	= projection->dtimes[ifo];       //offset time for detector ifo
      toff -= projection->dtimes[0];         //reference time is TOA at IFO0

      //store the timedelay to each IFO for use elsewhere
      projection->deltaT[ifo] = toff;

      //calculate frequency domain waveforms:
      freq  = fmin;							// initialize frequency
      df    = 1./Tobs;

      //Use recursion relationship for cos(Phase) & sin(Phase)
      trigArgument = LAL_TWOPI*toff;
      dim = trigArgument*df;
      trigArgument = trigArgument*freq+phi0;
      cosPhase = cos(trigArgument);
      sinPhase = sin(trigArgument);

      /* Incremental values, using cos(theta) - 1 = -2*sin(theta/2)^2 */
      dre = 0.5*dim;
      dim = -sin(dim);
      dre = sin(dre);
      dre = -2.0*dre*dre;

      //for(i=0; i<iend; i++)
      for(i=istart; i<iend; i++)
      {
         re = 2*i;
         im = re+1;

         projection->expPhase[ifo][re] = cosPhase;
         projection->expPhase[ifo][im] = sinPhase;

         if(ifo!=0)recursive_phase_evolution(dre, dim, &cosPhase, &sinPhase);
      }

      projection->Fplus[ifo]  = Fplus;
      projection->Fcross[ifo] = Fcross;

   }// end loop over detectors

}


void waveformProject(struct Data *data, struct Network *projection, double *extParams, double **response, double **h, double fmin, double fmax)
{
  double hpi, hpr;
  double hci, hcr;
  double Fplus, Fcross;

  double sn, cn;

  int i, id, real, imag;

  double scale = extParams[5];

  int n = data->N;
  int NI = data->NI;

  int imin=fmin*data->Tobs;
  int imax=fmax*data->Tobs;
  int imin2=2*imin;
  int imax2=2*imax;

  for(id = 0; id<NI; id++)
  {

    Fplus  = projection->Fplus[id]*scale;
    Fcross = projection->Fcross[id]*scale;

    for(i=0; i<imin2; i++) response[id][i]=0.0;
    for(i=imax2; i<n; i++) response[id][i]=0.0;

    for(i=imin; i<imax; i++)
    {
      real = 2*i;
      imag = real + 1;

      cn = projection->expPhase[id][real];
      sn = projection->expPhase[id][imag];

      //plus polarization
      hpr = (h[0][real]*cn - h[0][imag]*sn);
      hpi = (h[0][real]*sn + h[0][imag]*cn);

      //cross polarization
      hcr = (h[1][real]*cn - h[1][imag]*sn);
      hci = (h[1][real]*sn + h[1][imag]*cn);
      
      response[id][real] = (Fplus*hpr + Fcross*hcr);
      response[id][imag] = (Fplus*hpi + Fcross*hci);
    }
  }   // end loop over detectors
}

void combinePolarizations(struct Data *data, struct Wavelet **geo, double **h, double *extParams, int Npol)
{
  int i,n;
  double epsilon; //ellipticity parameter
  
  int N = data->N;
  
  //Elliptical polarization
  if(data->polarizationFlag)
  {
    epsilon = extParams[3];
    for(n=0; n<N/2; n++)
    {
      int re = 2*n;
      int im = re+1;
      
      //h+
      h[0][re] = geo[0]->templates[re];
      h[0][im] = geo[0]->templates[im];
      
      //hx = -i * epsilon * h+
      h[1][re] = -epsilon*h[0][im];
      h[1][im] =  epsilon*h[0][re];
    }
  }
  //Generic polarization
  else
  {
    for(i=0; i<Npol; i++)
    {
      for(n=0; n<N; n++)
      {
        h[i][n] = geo[i]->templates[n];
      }
    }
  }
}


void compute_reconstructed_plus_and_cross(struct Data *data, struct Network *projection, double *extParams, double **hrecPlus, double **hrecCross, double *geo, double fmin, double fmax)
{
   double hpi, hpr, hci, hcr;

   double df, freq;
   double sn, cn;

   int i, id;

   double ecc      = extParams[3];
   double scale    = extParams[5];

   int n = data->N;
   double Tobs = data->Tobs;
   int NI = data->NI;
   fmin = data->fmin;
   fmax = data->fmax;

   df = 1./(Tobs);

   for(id = 0; id<NI; id++)
   {

      freq  = 0.0;              // initialize frequency

      for(i=0; i<n/2; i++)
      {
         if((freq< fmin) || (freq > fmax))        // frequency below LIGO band or above termination point
         {
            hrecPlus[id][2*i] = 0.0;
            hrecPlus[id][2*i+1] = 0.0;
            hrecCross[id][2*i] = 0.0;
            hrecCross[id][2*i+1] = 0.0;
         }
         else                // frequency detectable and present
         {
            cn = projection->expPhase[id][2*i];
            sn = projection->expPhase[id][2*i+1];
            
            hpr = scale*(geo[2*i]*cn - geo[2*i+1]*sn);
            hpi = scale*(geo[2*i]*sn + geo[2*i+1]*cn);
            
            hcr = -ecc*hpi;
            hci = ecc*hpr;
            
            hrecPlus[id][2*i]   = hpr;
            hrecPlus[id][2*i+1] = hpi;
            hrecCross[id][2*i]   = hcr;
            hrecCross[id][2*i+1] = hci;
            
         }
         freq += df;
      }
   }   // end loop over detectors
}
