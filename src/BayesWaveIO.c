/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#include <fftw3.h>

#include "version.h"
#include "BayesLine.h"
#include "BayesWave.h"
#include "BayesWaveIO.h"
#include "BayesWaveMath.h"
#include "BayesWaveModel.h"
#include "BayesWaveWavelet.h"

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

/* ********************************************************************************** */
/*                                                                                    */
/*                        Data handling and injection routines                        */
/*                                                                                    */
/* ********************************************************************************** */

static void Lorentzian(double *Slines, double Tobs, lorentzianParams *lines, int ii, int N)
{
  int i;
  double f2,f4;
  double deltf;
  double freq,fsq, x, z, deltafmax, spread;
  double amplitude;
  int istart, istop, imid, idelt;

  double A = lines->A[ii];
  double Q = lines->Q[ii];
  double f = lines->f[ii];


  // here we figure out how many frequency bins are needed for the line profile
  imid = (int)((f)*Tobs);
  spread = (1.0e-2*Q);

  if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/50
  deltafmax = f/spread;
  deltf = 8.0*deltafmax;
  idelt = (int)(deltf*Tobs)+1;


  istart = imid-idelt;
  istop = imid+idelt;
  if(istart < 0)  istart = 0;
  if(istop > N/2) istop = N/2;



  // add or remove the old line
  f2=f*f;
  f4=f2*f2;
  amplitude = A*f4;//(f2*Q*Q);
  for(i=istart; i<istop; i++)
  {
    freq = (double)i/Tobs;
    fsq = freq*freq;
    x = fabs(f-freq);
    z = 1.0;
    if(x > deltafmax) z = exp(-(x-deltafmax)/deltafmax);

    //Slines[i] += z*amplitude/(fsq*(fsq-f2)*(fsq-f2));
    Slines[i] += z*amplitude/(f2*fsq+Q*Q*(fsq-f2)*(fsq-f2));
  }

}

REAL8TimeSeries *readTseries(CHAR *cachefile, CHAR *channel, LIGOTimeGPS start, REAL8 length)
{
  LALStatus status;
  memset(&status,0,sizeof(status));
  LALCache *cache = NULL;
  LALFrStream *stream = NULL;
  REAL8TimeSeries *out = NULL;

  cache  = XLALCacheImport( cachefile );
  int err;
  err = *XLALGetErrnoPtr();
  if(cache==NULL) {fprintf(stderr,"ERROR: Unable to import cache file \"%s\",\n       XLALError: \"%s\".\n",cachefile, XLALErrorString(err)); exit(-1);}
  stream = XLALFrStreamCacheOpen( cache );
  if(stream==NULL) {fprintf(stderr,"ERROR: Unable to open stream from frame cache file\n"); exit(-1);}
  out = XLALFrStreamInputREAL8TimeSeries( stream, channel, &start, length , 0 );
  if(out==NULL) fprintf(stderr,"ERROR: unable to read channel %s from %s at time %i\nCheck the specified data duration is not too long\n",channel,cachefile,start.gpsSeconds);
  XLALDestroyCache(cache);
  LALFrClose(&status,&stream);
  return out;
}

void InjectFromMDC(ProcessParamsTable *commandLine, LALInferenceIFOData *IFOdata, double *SNR)
{

  /* Read time domain WF present in an mdc frame file, FFT it and inject into the frequency domain stream */

  char mdcname[]="GW";
  char **mdc_caches=NULL;
  char **mdc_channels=NULL;
  ProcessParamsTable * ppt=commandLine;

  UINT4 nIFO=0;
  int i=0;
  UINT4 j=0;
  LALInferenceIFOData *data=IFOdata;
  REAL8 prefactor =1.0;
  ppt=LALInferenceGetProcParamVal(commandLine,"--MDC-prefactor");
  if (ppt){

    prefactor=atof(ppt->value);
    fprintf(stdout,"Using prefactor=%f to scale the MDC injection\n",prefactor);
  }
  REAL8 tmp=0.0;
  REAL8 net_snr=0.0;
  while (data) {nIFO++; data=data->next;}
  UINT4 Nmdc=0,Nchannel=0;

  ppt=LALInferenceGetProcParamVal(commandLine,"--MDC-cache");
  if (!ppt){

    fprintf(stderr,"You must provide the path of an MDC lal cache file for each IFO, using --MDC-cache [ XXX, YYY, ZZZ]\n");
    exit(1);

  }
  ppt=LALInferenceGetProcParamVal(commandLine,"--inj");
  if (ppt){

    fprintf(stderr,"You cannot use both injfile (--inj) and MDCs (--MDC-cache) Exiting... \n");
    exit(1);

  }

  ppt=LALInferenceGetProcParamVal(commandLine,"--MDC-cache");
  LALInferenceParseCharacterOptionString(ppt->value,&mdc_caches,&Nmdc);

  if (Nmdc!= nIFO){
    fprintf(stderr, "You have to provide an MDC cache file for each IFO\n");
    exit(1);
  }
  else printf("got %i ifos and %i MDC cache files\n",nIFO,Nmdc);


  ppt=LALInferenceGetProcParamVal(commandLine,"--MDC-channel");
  if (ppt){

    LALInferenceParseCharacterOptionString(ppt->value,&mdc_channels,&Nchannel);
    if (Nchannel!=Nmdc){
      fprintf(stderr,"You must provide a channel name for eache mdc frame, using --MDC-channel [X, Y, Z] . Exiting...\n");
      exit(1);
    }
  }
  else{
    fprintf(stdout,"WARNING: You did not provide the name(s) of channel(s) to use with the injection mdc. Using the default which may not be what you want!\n");
    mdc_channels=  malloc((nIFO+1)*sizeof(char*));
    data=IFOdata;
    i=0;
    while (data){
      mdc_channels[i] =  malloc(512*sizeof(char));
      if(!strcmp(data->name,"H1")) {
        sprintf(mdc_channels[i],"H1:%s-H",mdcname);}
      else if(!strcmp(data->name,"L1")) {
        sprintf(mdc_channels[i],"L1:%s-H",mdcname); }
      else if(!strcmp(data->name,"V1")) {
        sprintf(mdc_channels[i],"V1:%s-16K",mdcname);}
      data=data->next;
      i++;

    }
  }

  LIGOTimeGPS epoch=IFOdata->timeData->epoch;
  REAL8 deltaT=IFOdata->timeData->deltaT ;
  int seglen=IFOdata->timeData->data->length;
  REAL8 SampleRate=4096.0,SegmentLength=0.0;
  if(LALInferenceGetProcParamVal(commandLine,"--srate")) SampleRate=atof(LALInferenceGetProcParamVal(commandLine,"--srate")->value);
  SegmentLength=(REAL8) seglen/SampleRate;

  REAL8TimeSeries * timeData=NULL;
  REAL8TimeSeries * windTimeData=(REAL8TimeSeries *)XLALCreateREAL8TimeSeries("WindMDCdata",&epoch,0.0,deltaT,&lalDimensionlessUnit,(size_t)seglen);
  COMPLEX16FrequencySeries* injF=(COMPLEX16FrequencySeries *)XLALCreateCOMPLEX16FrequencySeries("injF",&IFOdata->timeData->epoch,0.0,IFOdata->freqData->deltaF,&lalDimensionlessUnit,  IFOdata->freqData->data->length);

  if(!injF) {
    fprintf(stdout,"Unable to allocate memory for injection buffer\n");
    fflush(stdout);
    exit(1);
  }

  REAL4 WinNorm = sqrt(IFOdata->window->sumofsquares/IFOdata->window->data->length);

  data=IFOdata;
  i=0;
  UINT4 lower = (UINT4)ceil(data->fLow / injF->deltaF);
  UINT4 upper = (UINT4)floor(data->fHigh /injF-> deltaF);

  /* Inject into FD data stream and calculate optimal SNR */
  while(data){

    char foutname[MAXSTRINGSIZE]="";
    sprintf(foutname,"MDC_freq_%s_%d",data->name,epoch.gpsSeconds);
    FILE * fout = fopen(foutname,"w");

    char outname[MAXSTRINGSIZE]="";
    sprintf(outname,"MDC_time_%s_%d",data->name,epoch.gpsSeconds);
    FILE * out = fopen(outname,"w");

    tmp=0.0;

    /* Read MDC frame */
    timeData=readTseries(mdc_caches[i],mdc_channels[i],epoch,SegmentLength);
    /* downsample */
    XLALResampleREAL8TimeSeries(timeData,1.0/SampleRate);
    /* window timeData and store it in windTimeData */
    XLALDDVectorMultiply(windTimeData->data,timeData->data,IFOdata->window->data);

    for(j=0;j< timeData->data->length;j++)
      fprintf(out,"%lf %10.10e %10.10e %10.10e \n",epoch.gpsSeconds + j*deltaT,data->timeData->data->data[j],data->timeData->data->data[j]+timeData->data->data[j],timeData->data->data[j]);
    fclose(out);

    /* set the whole seq to 0 */
    for(j=0;j<injF->data->length;j++) injF->data->data[j]*=0.0;//+1j*0.0;

    /* FFT */
    XLALREAL8TimeFreqFFT(injF,windTimeData,IFOdata->timeToFreqFFTPlan);


    for(j=lower;j<upper;j++){
      fprintf(fout,"%lf %10.10e %10.10e %10.10e\n", j*injF->deltaF,creal(injF->data->data[j])/WinNorm,cimag(injF->data->data[j])/WinNorm,data->oneSidedNoisePowerSpectrum->data->data[j]);
      //injF ->data->data[j]/=sqrt(data->window->sumofsquares / data->window->data->length);
      windTimeData->data->data[j] /= sqrt(data->window->sumofsquares / data->window->data->length);

      /* Add data in freq stream */
      data->freqData->data->data[j]+=crect(prefactor *creal(injF->data->data[j])/WinNorm,prefactor *cimag(injF->data->data[j])/WinNorm);
      tmp+= prefactor*prefactor*(creal(injF ->data->data[j])*creal(injF ->data->data[j])/WinNorm/WinNorm+cimag(injF ->data->data[j])*cimag(injF ->data->data[j])/WinNorm/WinNorm)/data->oneSidedNoisePowerSpectrum->data->data[j];
    }

    tmp*=2.*injF->deltaF;
    printf("Injected SNR %.3f in IFO %s from MDC \n",sqrt(2*tmp),data->name);
    data->SNR=sqrt(2*tmp);
    net_snr+=2*tmp;
    fclose(fout);
    SNR[i] = sqrt(2*tmp);

    i++;
    data=data->next;
  }
  printf("Injected network SNR %.3f from MDC\n",sqrt(net_snr));
  SNR[i]=sqrt(net_snr);
}


void BayesWaveInjection(ProcessParamsTable *commandLine, struct Data *data, struct Chain *chain, struct Prior *prior, double **psd, int *NC)
{
  int i,n,ifo;

  int NCflag=0;

  int N  = data->N;
  int NI = data->NI;

  int Nsample;

  struct Model *model = malloc(sizeof(struct Model));

  int signalFlag = data->signalFlag;
  int glitchFlag = data->glitchFlag;

  char filename[MAXSTRINGSIZE];
  char burnrows[100000];

  //User-friendly arrays for PSD, signal, and injected waveform
  double **hinj = malloc(NI*sizeof(double*));
  double **ginj = malloc(NI*sizeof(double*));

  for(ifo=0; ifo<NI; ifo++)
  {
    hinj[ifo] = malloc(N*sizeof(double));
    ginj[ifo] = malloc(N*sizeof(double));
  }


  //Command line options
  ProcessParamsTable *ppt = NULL;

  char injmodel[MAXSTRINGSIZE];
  char injname[MAXSTRINGSIZE];
  char injpath[MAXSTRINGSIZE];

  //which model?
  ppt = LALInferenceGetProcParamVal(commandLine, "--BW-inject");
  snprintf(injmodel,MAXSTRINGSIZE,"%s",ppt->value);

  //name of chain file?
  ppt = LALInferenceGetProcParamVal(commandLine, "--BW-injName");
  if(ppt) snprintf(injname,MAXSTRINGSIZE,"%s_",ppt->value);
  else injname[0]=0;

  //path to chain file?
  ppt = LALInferenceGetProcParamVal(commandLine, "--BW-path");
  if(ppt)snprintf(injpath,MAXSTRINGSIZE,"%s",ppt->value);
  else sprintf(injpath,"./chains");

  //which sample from chain file?
  ppt = LALInferenceGetProcParamVal(commandLine, "--BW-event");
  if(ppt) Nsample = atoi(ppt->value);
  else Nsample = 200000;

  FILE *runFile=NULL;
  FILE *snrFile=NULL;

  //Injecting glitch
  if(strcmp(injmodel, "glitch") == 0)
  {
    data->glitchFlag = 1;
    initialize_model(model, data, prior, psd, chain->seed);
    //for(ifo=0; ifo<NI; ifo++)model->Sn[ifo]=1.0;

    FILE **glitchParams = malloc(NI*sizeof(FILE *));

    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"%s/%sglitch_params_%s.dat.0",injpath,injname,data->ifos[ifo]);
      glitchParams[ifo] = fopen(filename,"r");
    }

    //Get sample from chain to inject
    sprintf(filename,"%sbayeswave.run",data->runName);
    runFile = fopen(filename,"a");
    fprintf(runFile, " ========== Glitch Injection ========= \n");

    for(ifo=0; ifo<NI; ifo++)
    {
      for(n=0; n<Nsample+1; n++) fgets(burnrows,100000,glitchParams[ifo]);


      fprintf(runFile,"%s\n",burnrows);
      rewind(glitchParams[ifo]);

      for(n=0; n<Nsample; n++) fgets(burnrows,100000,glitchParams[ifo]);

    }
    fclose(runFile);
    parse_glitch_parameters(data, model, glitchParams, ginj);

    double GNR[NI];

    sprintf(filename,"%ssnr/snr.dat",data->runName);
    snrFile=fopen(filename,"w");

    for(ifo=0; ifo<NI; ifo++)
    {
      GNR[ifo] = detector_snr((int)floor(data->fmin*data->Tobs), (int)floor(data->fmax*data->Tobs), model->glitch[ifo]->templates, model->invSnf[ifo], 1.0);
      fprintf(snrFile,"%g ",GNR[ifo]);
    }
    fprintf(snrFile,"\n");
    fclose(snrFile);


    for(ifo=0; ifo<NI; ifo++)
    {
        fprintf(stdout, "g%iSNR=%.0f\n",  ifo, GNR[ifo]);
        if(GNR[ifo]>100.) NCflag++;
        if(GNR[ifo]>200.) NCflag++;
    }




    //Add model to data
    for(ifo=0; ifo<NI; ifo++) for (i=0; i<data->N; i++) data->s[ifo][i] += ginj[ifo][i];

    for(ifo=0; ifo<NI; ifo++) fclose(glitchParams[ifo]);

    free_model(model, data, prior);
    free(glitchParams);

  }

  //Injecting signal
  if(strcmp(injmodel, "signal") == 0)
  {
    data->signalFlag = 1;
    initialize_model(model, data, prior, psd, chain->seed);
    //for(ifo=0; ifo<NI; ifo++)model->Sn[ifo]=1.0;

    FILE **signalParams = malloc(data->Npol*sizeof(FILE *));
    
    //File containing signal-model waveform parameters
    sprintf(filename,"%sbayeswave.run",data->runName);

    runFile = fopen(filename,"a");
    fprintf(runFile, " ========== Signal Injection ========= \n");

    for(ifo=0; ifo<data->Npol; ifo++)
    {
      sprintf(filename,"%s/%ssignal_params_h%i.dat.0",injpath,injname,ifo);
      
      signalParams[ifo] = fopen(filename,"r");
      
      for(n=0; n<Nsample+1; n++) fgets(burnrows,100000,signalParams[ifo]);
      
      fprintf(runFile,"%s\n",burnrows);
      rewind(signalParams[ifo]);
      
      for(n=0; n<Nsample; n++) fgets(burnrows,100000,signalParams[ifo]);
    }
    fclose(runFile);
    parse_signal_parameters(data, model, signalParams, hinj);

    double SNRnet;
    double SNRifo[NI];

    SNRnet = network_snr((int)floor(data->fmin*data->Tobs), (int)floor(data->fmax*data->Tobs), model->response, model->invSnf, data->NI);
    for(ifo=0; ifo<NI; ifo++) SNRifo[ifo] = detector_snr((int)floor(data->fmin*data->Tobs), (int)floor(data->fmax*data->Tobs), model->response[ifo], model->invSnf[ifo], 1.0);
    if(SNRnet>100.) NCflag++;
    if(SNRnet>200.) NCflag++;
    sprintf(filename,"%ssnr/snr.dat",data->runName);
    snrFile=fopen(filename,"w");

    if(SNRifo[0]>SNRifo[1])
      fprintf(snrFile,"%g ",SNRifo[1]);
    else
      fprintf(snrFile,"%g ",SNRifo[0]);

    fprintf(snrFile,"%g\n",SNRnet);
    fclose(snrFile);

    fprintf(stdout, "SNR = %.0f\n",SNRnet);
    for(ifo=0; ifo<NI; ifo++)
      fprintf(stdout, "%s SNR = %.0f\n",  data->ifos[ifo], SNRifo[ifo]);


    //Add model to data
    for(ifo=0; ifo<NI; ifo++) for (i=0; i<data->N; i++) data->s[ifo][i] += hinj[ifo][i];

    for(n=0; n<data->Npol; n++)fclose(signalParams[n]);
    free(signalParams);
    free_model(model, data, prior);

  }
  fprintf(stdout,"\n");

  for(ifo=0; ifo<NI; ifo++)
  {
    free(hinj[ifo]);
    free(ginj[ifo]);
  }
  free(hinj);
  free(ginj);

  if(data->cleanModelFlag==0 && NCflag>0)
  {
    fprintf(stdout,"BayesWave internal injection with SNR>100\n");
    fprintf(stdout,"Cleaning phase already disabled\n");
    fprintf(stdout,"Manually increasing number of chains to %i\n\n",*NC+5);
    if(NCflag>0)*NC+=5;
    if(NCflag>1)*NC+=5;
      
  }

  data->signalFlag = signalFlag;
  data->glitchFlag = glitchFlag;
}


void getChannels(ProcessParamsTable *commandLine, char **channel)
{
    ProcessParamsTable *ppt = NULL;
    char ifo[4];
    char arg[MAXSTRINGSIZE];

    ProcessParamsTable *argument=commandLine;

    int nifo=0;

    fprintf(stdout," ============= Channels ==============\n");
    while(argument)
    {
        if(!strcmp(argument->param,"--ifo") || !strcmp(argument->param,"--IFO"))
        {
            //Get name of one of the interferferometers (channel name is preceeded by IFO name)
            ppt = LALInferenceGetProcParamVal(commandLine, "--ifo");
            snprintf(ifo,4,"%s",argument->value);
            fprintf(stdout,"Detector %s: ",ifo);

            //Get channel name of interferometers
            sprintf(arg,"--%s-channel",ifo);

            ppt = LALInferenceGetProcParamVal(commandLine,arg);
            if(ppt)
            {
                channel[nifo] = (char*)malloc(strlen(ppt->value) + 1 * sizeof(char));
                strcpy(channel[nifo], ppt->value);

                //This removes the ifo name from the channel string
                memmove(channel[nifo], channel[nifo]+3, strlen(channel[nifo]));
            }
            else
            {
                sprintf(arg,"%s:Simulated",ifo);
                channel[nifo]=arg;
            }
            fprintf(stdout,"channel %s\n",channel[nifo]);
            nifo++;
        }
        argument=argument->next;
    }
}

/* ********************************************************************************** */
/*                                                                                    */
/*                                   Output Files                                     */
/*                                                                                    */
/* ********************************************************************************** */

void write_gnuplot_script_header(FILE *script, char modelname[])
{
  char filename[MAXSTRINGSIZE];
  sprintf(filename,"waveforms/%s_waveforms_animate.gpi",modelname);
  script=fopen(filename,"w");
  fprintf(script,"set terminal gif giant enhanced size 800,600 animate delay 1 \n");
  fprintf(script,"set output \"%s_waveforms.gif\"                     \n",modelname);
  fprintf(script,"                                   \n");
  //fprintf(script,"set xrange[%g:%g]                  \n", -0.02, +0.02);
  //fprintf(script,"set yrange[-20:20]                 \n");
  //fprintf(script,"set xtics 0.02                     \n");
  fprintf(script,"                                   \n");
  fprintf(script,"set xlabel \"time (s)\"            \n");
  fprintf(script,"set ylabel \"whitened strain\"     \n");
  fprintf(script,"                                   \n");
}

void write_gnuplot_script_frame (FILE *script, char modelname[], int signal, int glitch, int cycle, int NI)
{
  int i;

  char datafile[MAXSTRINGSIZE];
  sprintf(datafile,"%s_data_%i.dat.%i", modelname,cycle,0);


  fprintf(script,"set multiplot  \n");
  fprintf(script,"set size 0.5,%f \n",1./(double)NI);

  for(i=0; i<NI; i++)
  {
    sprintf(datafile,"%s_data_%i.dat.%i", modelname,cycle,i);

    fprintf(script,"set origin 0,%f \n",(double)i/(double)NI);
    fprintf(script,"set title \"IFO %i\" \n", i);
    fprintf(script,"p ");
    fprintf(script,"\"%s\" u 1:2 title \"data\" w l ls 1 lw 2, ",datafile);
    if(signal)fprintf(script,"\"%s_wave_%i.dat.%i\" u 1:2 title \"signal model\" w l ls 2 lw 2, " ,modelname,cycle,i);
    if(glitch)fprintf(script,"\"%s_glitch_%i.dat.%i\" u 1:2 title \"glitch model\" w l ls 3 lw 2 ",modelname,cycle,i);
    fprintf(script,"\n");
  }

  fprintf(script,"unset multiplot \n");
  fflush(script);
}

void write_bayesline_gnuplot_script_frame(FILE *script, char modelname[], int cycle)
{
  fprintf(script,"set multiplot  \n");
  fprintf(script,"set size 1,0.5 \n");
  fprintf(script,"set origin 0,0.5 \n");
  fprintf(script,"set title \"H1\" \n");
  fprintf(script,"p ");
  fprintf(script,"\"%s_psd_%i.dat.%i\" u 1:2 title \"residual\" w l ls 1 lw 2, "     ,modelname,cycle,0);
  fprintf(script,"\"%s_psd_%i.dat.%i\" u 1:3 title \"spline model\" w l ls 2 lw 2, " ,modelname,cycle,0);
  fprintf(script,"\"%s_psd_%i.dat.%i\" u 1:4 title \"line model\" w l ls 3 lw 2 "    ,modelname,cycle,0);
  fprintf(script,"\n");
  fprintf(script,"set origin 0,0\n");
  fprintf(script,"set title \"L1\" \n");
  fprintf(script,"p ");
  fprintf(script,"\"%s_psd_%i.dat.%i\" u 1:2 title \"residual\" w l ls 1 lw 2, "     ,modelname,cycle,1);
  fprintf(script,"\"%s_psd_%i.dat.%i\" u 1:3 title \"spline model\" w l ls 2 lw 2, " ,modelname,cycle,1);
  fprintf(script,"\"%s_psd_%i.dat.%i\" u 1:4 title \"line model\" w l ls 3 lw 2 "    ,modelname,cycle,1);
  fprintf(script,"\n");
  fprintf(script,"unset multiplot \n");
  fflush(script);
}

void write_bayeswave_gnuplot_script_frame(FILE *script, char modelname[], double Tobs, double fmin, double fmax, int phase, int signal, int glitch, int cycle, int NI)
{
  int i;

  char datafile[MAXSTRINGSIZE];
  sprintf(datafile,"%s_data_%i.dat.%i", modelname,cycle,0);

  fprintf(script,"set multiplot  \n");
  fprintf(script,"set size 0.5,0.5 \n");

  fprintf(script,"set xrange[%g:%g]                  \n", fmin, fmax);
  fprintf(script,"set yrange[%g:%g]                  \n", 1.0e-48, 1.0e-38);
  fprintf(script,"set xtics 100                       \n");
  fprintf(script,"set logscale y                     \n");
  fprintf(script,"                                   \n");
  fprintf(script,"set xlabel \"frequency (Hz)\"      \n");
  fprintf(script,"set ylabel \"PSD\"                 \n");

  for(i=0; i<NI; i++)
  {
    fprintf(script,"set origin 0,%f \n",0.5*(double)(1-i));
    fprintf(script,"set title \"IFO %i\" \n", i);
    fprintf(script,"p ");
    fprintf(script,"\"%s_psd_%i.dat.%i\" u 1:2 title \"residual\" w l ls 2, "     ,modelname,cycle,i);
    fprintf(script,"\"%s_psd_%i.dat.%i\" u 1:3 title \"spline model\" w l ls 1, " ,modelname,cycle,i);
    fprintf(script,"\"%s_psd_%i.dat.%i\" u 1:4 title \"line model\" w l ls 3 "    ,modelname,cycle,i);
    fprintf(script,"\n");
  }

  fprintf(script,"unset logscale y \n");
  fprintf(script,"\n");


  if(phase==0)
  {
    fprintf(script,"set xrange[%g:%g]\n",-Tobs+3.,1.);
    fprintf(script,"set xtics 1\n");
    //fprintf(script,"set yrange[-40:40]\n");
  }
  if(phase==1)
  {
    fprintf(script,"set xrange[%g:%g]\n", -1., 1.);
    fprintf(script,"set xtics 0.1\n");
    //fprintf(script,"set yrange[-20:20]\n");
  }

  fprintf(script,"set xrange[*:*]\n");
  fprintf(script,"set yrange[*:*]\n");

  fprintf(script,"                                   \n");
  fprintf(script,"set xlabel \"time (s)\"            \n");
  fprintf(script,"set ylabel \"whitened strain\"     \n");


  for(i=0; i<NI; i++)
  {

    sprintf(datafile,"%s_data_%i.dat.%i", modelname,cycle,i);

    fprintf(script,"set origin 0.5,%f \n",0.5*(double)(1-i));
    fprintf(script,"set title \"IFO %i\" \n",i);
    fprintf(script,"p ");
    fprintf(script,"\"%s\" u 1:2 title \"data\" w l ls 2, " ,datafile);

    if(signal) fprintf(script,"\"<paste %s_data_%i.dat.%i %s_wave_%i.dat.%i\" u 1:($2-$4) title \"residual\" w l ls 1 ", modelname,cycle,i,modelname,cycle,i);
    if(glitch) fprintf(script,"\"<paste %s_data_%i.dat.%i %s_glitch_%i.dat.%i\" u 1:($2-$4) title \"residual\" w l ls 1 ", modelname,cycle,i,modelname,cycle,i);
    fprintf(script,"\n");
  }

  fprintf(script,"unset multiplot \n");
  fflush(script);
}

void write_evidence_gnuplot_script(FILE *script, char modelname[])
{
  fprintf(script,"reset\n");
  fprintf(script,"set terminal postscript color solid enhanced linewidth 2.0 \"Helvetica\" 24\n");
  fprintf(script,"\n");
  fprintf(script,"unset key\n");
  fprintf(script,"set border 3 linewidth 0.5\n");
  fprintf(script,"set nox2tics\n");
  fprintf(script,"set noy2tics\n");
  fprintf(script,"set tics scale 2\n");
  fprintf(script,"set style fill solid 1.0 border lt -1\n");
  fprintf(script,"set xtics nomirror axis offset 0,0.3 scale 0.5\n");
  fprintf(script,"set ytics nomirror\n");
  fprintf(script,"\n");
  fprintf(script,"set mytics\n");
  fprintf(script,"\n");
  fprintf(script,"set xlabel \"Model\"\n");
  fprintf(script,"set ylabel \"p( Model | d )\"\n");
  fprintf(script,"set logscale y\n");
  fprintf(script,"set format y \"10^{%%T}\"\n");
  fprintf(script,"\n");
  fprintf(script,"set xtics font \"Helvetica,20\"\n");
  fprintf(script,"set ytics font \"Helvetica,20\"\n");
  fprintf(script,"\n");
  fprintf(script,"set style data histograms\n");
  fprintf(script,"set style histogram cluster gap 1\n");
  fprintf(script,"\n");
  fprintf(script,"\n");
  fprintf(script,"set output \"evidence.eps\"\n");
  fprintf(script,"p \"%sevidence.dat\" using 3:xtic(1)\n",modelname);
}

void write_gnuplot_psd_animation(struct Data *data, char modelname[], int frame)
{
  int n;
  char **ifos = data->ifos;
  int nifo = data->NI;

  char filename[MAXSTRINGSIZE];
  sprintf(filename,"waveforms/%s_bayeswave_animation.gpi", modelname);
  FILE *framefile = fopen(filename,"w");
  sprintf(filename,"animation/%s_bayeswave_animation.mp4", modelname);

  fprintf(framefile,"!mkdir -p animation\n");
  fprintf(framefile,"!rm %s\n",filename);
  fprintf(framefile,"\n");
  fprintf(framefile,"nmax=%i\n",frame);
  fprintf(framefile,"nifo=%i\n",nifo);
  fprintf(framefile,"xsize=%i\n",1280);
  fprintf(framefile,"ysize=%i\n",230);
  fprintf(framefile,"rescale=%i\n",4);
  fprintf(framefile,"\n");
  fprintf(framefile,"set terminal pngcairo fontscale 1.2 size xsize,nifo*ysize\n");
  fprintf(framefile,"\n");
  fprintf(framefile,"set logscale\n");
  fprintf(framefile,"set tics scale 2 nomirror\n");
  fprintf(framefile,"set border 3\n");
  fprintf(framefile,"\n");
  fprintf(framefile,"psdmin = 1e-48\n");
  fprintf(framefile,"psdmax = 1e-38\n");
  fprintf(framefile,"tmin = -0.5\n");
  fprintf(framefile,"tmax = 0.5\n");
  fprintf(framefile,"hmin = -10\n");
  fprintf(framefile,"hmax = 10\n");
  fprintf(framefile,"\n");
  fprintf(framefile,"do for [N=0:nmax] {\n");
  fprintf(framefile,"     outfile = sprintf(\"animation/bayeswave%%03.0f.png\",N)\n");
  fprintf(framefile,"     set output outfile\n");
  fprintf(framefile,"\n");
  fprintf(framefile,"     set multiplot layout nifo,2\n");
  fprintf(framefile,"\n");

  for(n=0; n<nifo; n++)
  {
    fprintf(framefile,"     #%s\n",ifos[n]);
    fprintf(framefile,"     set title '%s'\n",ifos[n]);
    fprintf(framefile,"     set logscale\n");
    fprintf(framefile,"     set format \"10^{%%T}\"\n");
    fprintf(framefile,"     set xlabel 'f'\n");
    fprintf(framefile,"     set ylabel '|h|^2'\n");
    fprintf(framefile,"     plot [*:*] [psdmin:psdmax] '%s_psd_%s_'.N.'.dat' u 1:2 w l lc 'gray' notitle, '' u 1:($3+$4) w l lc 'black' lw 2 notitle\n", modelname,ifos[n]);
    fprintf(framefile,"\n");
    fprintf(framefile,"     unset logscale\n");
    fprintf(framefile,"     set format '%%g'\n");
    fprintf(framefile,"     set xlabel 't'\n");
    fprintf(framefile,"     set ylabel 'h'\n");
    fprintf(framefile,"     plot [tmin:tmax] [hmin:hmax] '%s_data_%s_'.N.'.dat' w l lc 'gray' notitle",modelname,ifos[n]);
    if(data->glitchFlag) fprintf(framefile,", '%s_glitch_%s_'.N.'.dat' w l lc rgb '#a6611a' lw 2 title 'glitch'",modelname,ifos[n]);
    if(data->signalFlag) fprintf(framefile,", '%s_wave_%s_'.N.'.dat' w l lc rgb '#7b3294' lw 2 title 'signal'",modelname,ifos[n]);
    fprintf(framefile,"\n");
    fprintf(framefile,"\n");
  }

  fprintf(framefile,"\n");
  fprintf(framefile,"     unset multiplot\n");
  fprintf(framefile,"}\n");
  fprintf(framefile,"\n");
  fprintf(framefile,"CMD='ffmpeg -r 8 -f image2 -s '.xsize*rescale.'x'.rescale*nifo*ysize.' -start_number 0 -i animation/bayeswave%%03d.png -vframes '.nmax.' -vcodec libx264 -crf 25  -pix_fmt yuv420p %s'\n",filename);
  fprintf(framefile,"\n");
  fprintf(framefile,"system(CMD)\n");
  fprintf(framefile,"\n");
  fprintf(framefile,"!rm animation/*.png\n");
  fprintf(framefile,"\n");

  fclose(framefile);
}

void print_bayesline_spectrum(char filename[], double *f, double *power, double *Sbase, double *Sline, int N)
{
  int i;
  FILE *waveout = fopen(filename,"w");

  for(i=0; i<N; i++) fprintf(waveout,"%.16g %.16g %.16g %.16g\n", f[i], power[i], Sbase[i], Sline[i]);

  fclose(waveout);
}


void print_run_stats(FILE *fptr, struct Data *data, struct Chain *chain)
{
  fprintf(fptr, " ============ Run Settings ===========\n");
  fprintf(fptr, "  number of detectors             %i\n",   data->NI);
  fprintf(fptr, "  trigger time (s)                %s\n",   LALInferenceGetProcParamVal(data->commandLine,"--trigtime")->value);
  fprintf(fptr, "  duration of data (s)            %.0f\n", data->Tobs);
  fprintf(fptr, "  duration of window (s)          %.1f\n", data->Twin);
  fprintf(fptr, "  sampling rate (Hz)              %.0f\n", (double)data->N/data->Tobs);
  fprintf(fptr, "  number of samples               %i\n",   data->N);
  fprintf(fptr, "  number of iterations            %i\n",   chain->count);
  fprintf(fptr, "  number of burn-in iterations    %i\n",   chain->burn);
  fprintf(fptr, "  max N wavelets                  %i\n",   data->Dmax);
  fprintf(fptr, "  max Q for wavelets              %.0f\n", data->Qmax);
  fprintf(fptr, "  number of chains                %i\n",   chain->NC);
  fprintf(fptr, "  coldest chain temperature       %.0f\n", chain->tempMin);
  fprintf(fptr, "  initial temperature spacing     %.1f\n", chain->tempStep);
  fprintf(fptr, "  number of independent steps     %i\n",   chain->cycle);
  fprintf(fptr,"\n");
}

void print_version(FILE *fptr)
{
  fprintf(fptr, "\n");
  fprintf(fptr, " ========= BayesWave Version: ========\n\n");
  fprintf(fptr, "  Git remote origin: %s\n", GIT_URL);
  fprintf(fptr, "  Git version: %s\n", GIT_VER);
  fprintf(fptr, "  Git commit: %s\n", GIT_HASH);
  fprintf(fptr, "  Git commit author: %s\n",GIT_AUTHOR);
  fprintf(fptr, "  Git commit date: %s\n", GIT_DATE);
}
void print_run_flags(FILE *fptr, struct Data *data, struct Prior *prior)
{
  fprintf(fptr, " ============= Run Flags =============\n");

  fprintf(fptr, "  self checkpointing is .... ");
  if(data->checkpointFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  maximized logL is ........ ");
  if(data->searchFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  BayesLine PSD model is ... ");
  if(data->bayesLineFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  Stochastic model is ...... ");
  if(data->stochasticFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  cleaning phase is ........ ");
  if(data->cleanModelFlag)   fprintf(fptr, "ENABLED");
  else                       fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  glitch model is .......... ");
  if(!data->glitchFlag)      fprintf(fptr, "DISABLED");
  else if(!data->signalFlag) fprintf(fptr, "REQUIRED");
  else                       fprintf(fptr, "ENABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  signal model is .......... ");
  if(!data->signalFlag)      fprintf(fptr, "DISABLED");
  else if(!data->glitchFlag) fprintf(fptr, "REQUIRED");
  else                       fprintf(fptr, "ENABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  wavelet prior is ......... ");
  if(data->waveletPriorFlag) fprintf(fptr, "ENABLED");
  else                       fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  clustering prior is ...... ");
  if(data->clusterPriorFlag) fprintf(fptr, "(%i,%i,%i)",(int)prior->alpha, (int)prior->beta, (int)prior->gamma);
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  glitch amp prior is ...... ");
  if(data->amplitudePriorFlag) fprintf(fptr, "SNR*=%g",prior->gSNRpeak);
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  signal amp prior is ...... ");
  if(data->amplitudePriorFlag && data->signalAmplitudePriorFlag) fprintf(fptr, "SNR*=%g",prior->sSNRpeak);
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  ext. amp. update is ...... ");
  if(data->extrinsicAmplitudeFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  polarization constraint is ");
  if(data->polarizationFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  background prior is ...... ");
  if(data->backgroundPriorFlag && data->glitchFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  density proposal is ...... ");
  if(data->clusterProposalFlag)
  {
      if(data->logClusterProposalFlag) fprintf(fptr, "SNR^2/2");
      else                             fprintf(fptr, "exp(SNR^2/2)-1");
  }
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  orientation proposal is .. ");
  if(data->orientationProposalFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  amplitude proposal is .... ");
  if(data->amplitudeProposalFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  thermo. integration ...... ");
  if(data->splineEvidenceFlag) fprintf(fptr, "SPLINE");
  else fprintf(fptr, "TRAPEZOID");
  fprintf(fptr, "\n");

  fprintf(fptr, "  logL=const test is ....... ");
  if(data->constantLogLFlag) fprintf(fptr, "ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  PTMCMC adaptation is ..... ");
  if(data->adaptTemperatureFlag) fprintf(fptr,"ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "  gnuplot output is ........ ");
  if(data->gnuplotFlag) fprintf(fptr,"ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");
  
  fprintf(fptr, "  chirplet option is ....... ");
  if(data->chirpletFlag) fprintf(fptr,"ENABLED");
  else fprintf(fptr, "DISABLED");
  fprintf(fptr, "\n");

  fprintf(fptr, "\n");
}

void print_chain_files(struct Data *data, struct Chain *chain, struct Model **model, struct BayesLineParams ***bayesline, int ic)
{
  int n;
  int NI = data->NI;

  struct Model *model_x = model[chain->index[ic]];
  struct BayesLineParams **psd_x = bayesline[chain->index[ic]];

  //TODO "model" chain is very redundent
  //print "model" chain (logL, PSD parameters, etc.)
  print_model(chain->intChainFile[ic], data, chain, model_x);

  //print signal model parameters
  if(data->signalFlag) for(n=0; n<model_x->Npol; n++) print_signal_model(chain->intWaveChainFile[n][ic], model_x, n);

  //print glitch model parameters
  if(data->glitchFlag) for(n=0; n<NI; n++) print_glitch_model(chain->intGlitchChainFile[n][ic], model_x->glitch[n]);

  //print PSD parameters
  if(data->bayesLineFlag)
  {
    for(n=0; n<NI; n++)
    {
      print_line_model(chain->lineChainFile[ic][n],psd_x[n]);
      print_spline_model(chain->splineChainFile[ic][n],psd_x[n]);
    }
  }

  if(data->verboseFlag)fflush(chain->intChainFile[ic]);
}

void flush_chain_files(struct Data *data, struct Chain *chain, int ic)
{
  int n;
  int NP = data->Npol;
  int NI = data->NI;

  fflush(chain->intChainFile[ic]);

  //print signal model parameters
  if(data->signalFlag) for(n=0; n<NP; n++) fflush(chain->intWaveChainFile[n][ic]);

  //print glitch model parameters
  if(data->glitchFlag) for(n=0; n<NI; n++) fflush(chain->intGlitchChainFile[n][ic]);

  //print PSD parameters
  if(data->bayesLineFlag)
  {
    for(n=0; n<NI; n++)
    {
      fflush(chain->lineChainFile[ic][n]);
      fflush(chain->splineChainFile[ic][n]);
    }
  }
}

void print_model(FILE *fptr, struct Data *data, struct Chain *chain, struct Model *model)
{
  int ifo;
  int NI = data->NI;

  fprintf(fptr,"%d %f ", chain->mc, model->logL-data->logLc+model->logLnorm);
  fprintf(fptr,"%d ", model->signal[0]->size);
  for(ifo=0; ifo<NI; ifo++) fprintf(fptr,"%d ", model->glitch[ifo]->size);
  //for(ifo=0; ifo<NI; ifo++) fprintf(fptr, "%f ",model->Sn[ifo]);
  if(data->stochasticFlag) fprintf(fptr,"%f %f ",model->background->logamp,model->background->index);
  fprintf(fptr,"%f %f ",model->logL,model->logLnorm);
  fprintf(fptr,"\n");
}

void print_signal_model(FILE *fptr, struct Model *model, int n)
{
  int i,j,k;

  fprintf(fptr,"%i ",model->signal[n]->size);
  for(k=0; k<NE; k++) fprintf(fptr,"%.12g ", model->extParams[k]);
  for(j=1;j<=model->signal[n]->size; j++)  // print out first few sine-gaussians
  {
    i = model->signal[n]->index[j];

    //print all parameters
    for(k=0; k<model->signal[n]->dimension; k++) fprintf(fptr,"%.12g ", model->signal[n]->intParams[i][k]);
  }
  fprintf(fptr,"\n");
}

void print_glitch_model(FILE *fptr, struct Wavelet *glitch)
{
  int i,j,k;
  fprintf(fptr,"%i ",glitch->size);
  for(j=1;j<=glitch->size; j++)  // print out first few sine-gaussians
  {
    i = glitch->index[j];

    //print all parameters
    for(k=0; k<glitch->dimension; k++) fprintf(fptr,"%.12g ", glitch->intParams[i][k]);
  }
  fprintf(fptr,"\n");
}

void print_chain_status(struct Data *data, struct Chain *chain, struct Model **model, int searchFlag)
{
  int ifo;
  int NI = data->NI;

  double SNR;

  //print overal status
  fprintf(stdout, "RJMCMC: %i/%i (%i,%i)\n",chain->mc,chain->count,chain->burnFlag,searchFlag);


  SNR = 0.0;
  if(data->signalFlag) SNR = network_snr((int)floor(data->fmin*data->Tobs), (int)floor(data->fmax*data->Tobs), model[chain->index[0]]->response, model[chain->index[0]]->invSnf, data->NI);
  fprintf(stdout, "  logL=%f hSNR=%.0f ",  model[chain->index[0]]->logL - data->logLc, SNR);
  for(ifo=0; ifo<NI; ifo++)
  {
    SNR = 0.0;
    if(data->glitchFlag) SNR = detector_snr((int)floor(data->fmin*data->Tobs), (int)floor(data->fmax*data->Tobs), model[chain->index[0]]->glitch[ifo]->templates, model[chain->index[0]]->invSnf[ifo], 1.0);
    fprintf(stdout, "  g%iSNR=%.0f ",  ifo, SNR);
  }
  fprintf(stdout, "\n");


  //print intrinsic parameter stats
  fprintf(stdout, "  DIM: ");
  fprintf(stdout, "DGW=%d ",  model[chain->index[0]]->signal[0]->size);
  for(ifo=0; ifo<NI; ifo++) fprintf(stdout, "D%i=%d ", ifo, model[chain->index[0]]->glitch[ifo]->size);
  fprintf(stdout, "m0=%i m1=%i m2=%i m3=%i ", chain->mod0, chain->mod1, chain->mod2, chain->mod3);
  fprintf(stdout, "\n");
  fprintf(stdout, "  RJ:  ");
  fprintf(stdout, "rjacc=%.1e ", (double)chain->sacc/(double)(chain->scount));
  fprintf(stdout, "cacc=%.1e " , (double)chain->cacc/(double)(chain->ccount));
  fprintf(stdout, "dacc=%.1e " , (double)chain->dacc/(double)(chain->dcount));
  fprintf(stdout, "uacc=%.1e " , (double)chain->uacc/(double)(chain->ucount));
  fprintf(stdout, "\n");
  fprintf(stdout,"  INT: ");
  fprintf(stdout, "macc=%.1e " , (double)chain->macc/(double)(chain->mcount));
  fprintf(stdout, "facc=%.1e " , (double)chain->facc/(double)(chain->fcount));
  fprintf(stdout, "pacc=%.1e " , (double)chain->pacc/(double)(chain->pcount));
  fprintf(stdout, "\n");


  //print extrinsic parameter stats
  if(data->signalFlag)
  {
    fprintf(stdout, "  EXT: xacc=%.1e ",(double)chain->xacc/(double)chain->xcount);
    fprintf(stdout, "\n");
  }

  fflush(stdout);
}




void print_time_domain_waveforms(char filename[], double *h, int N, double *Snf, double Tobs, int imin, int imax, double tmin, double tmax)
{
  int i;
  double x,t;
  FILE *waveout = fopen(filename,"w");

  double *ht = malloc(N*sizeof(double));

  fftw_complex *hf = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);
  fftw_plan reverse = fftw_plan_dft_c2r_1d(N, hf, ht, FFTW_MEASURE);
  
  for (i=0; i < N; i++) ht[i] = 0.0;

  hf[0][0] = 0.0;
  hf[0][1] = 0.0;
  for(i=1; i< N/2; i++)
  {
    if(i>imin && i<imax)
    {
      x = sqrt(Snf[i]);
      hf[i][0] = h[2*i]/x;
      hf[i][1] = h[2*i+1]/x;
    }
    else
    {
      hf[i][0] = 0.0;
      hf[i][1] = 0.0;
    }
  }
  
  //get DC and Nyquist where FFTW wants them
  hf[N/2][0] = hf[0][1];
  hf[N/2][1] = hf[0][1] = 0.0;

  fftw_execute(reverse);
  
  double norm = sqrt((double)N);

  double t0 = Tobs-2.0;

  for(i=0; i<N; i++)
  {
    t = (double)(i)/(double)(N)*Tobs;
    if(t>=tmin && t<tmax)fprintf(waveout,"%.16g %.16g\n", t-t0, ht[i]/norm);
  }

  free(ht);
  fftw_destroy_plan(reverse);
  fftw_free(hf);
  
  fclose(waveout);
}

void print_colored_time_domain_waveforms(char filename[], double *h, int N, double Tobs, int imin, int imax, double tmin, double tmax)
{
  int i;
  double t;
  FILE *waveout = fopen(filename,"w");

  double *ht = malloc(N*sizeof(double));

  fftw_complex *hf = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);
  fftw_plan reverse = fftw_plan_dft_c2r_1d(N, hf, ht, FFTW_MEASURE);


  for(i=0; i<N; i++) ht[i] = 0.0;
  
  hf[0][0] = 0.0;
  hf[0][1] = 0.0;
  for(i=1; i< N/2; i++)
  {
    if(i>imin && i<imax)
    {
      hf[i][0] = h[2*i];
      hf[i][1] = h[2*i+1];
    }
    else
    {
      hf[i][0] = 0.0;
      hf[i][1] = 0.0;
    }
  }
  
  //get DC and Nyquist where FFTW wants them
  hf[N/2][0] = hf[0][1];
  hf[N/2][1] = hf[0][1] = 0.0;

  fftw_execute(reverse);
  
  double norm = sqrt((double)N);
  
  double t0 = Tobs-2.0;
  
  for(i=0; i<N; i++)
  {
    t = (double)(i)/(double)(N)*Tobs;
    if(t>=tmin && t<tmax)fprintf(waveout,"%.16g %.16g\n", t-t0, ht[i]/norm);
  }
  
  free(ht);
  fftw_destroy_plan(reverse);
  fftw_free(hf);
  
  fclose(waveout);
}


void print_time_domain_hdot(char filename[], double *h, int N, double *Snf, double Tobs, int imin, int imax, double tmin, double tmax)
{
  int i;
  double x,t;
  double xx, yy;
  double deltaF = 1./Tobs;
  FILE *waveout = fopen(filename,"w");

  // hdot = IFT(-2pi*i*f*h(f))

  double *ht = malloc(N*sizeof(double));
  
  fftw_complex *hf = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);
  fftw_plan reverse = fftw_plan_dft_c2r_1d(N, hf, ht, FFTW_MEASURE);
  
  for(i=0; i<N/2; i++)
  {
    hf[i][0] = -LAL_TWOPI*h[2*i];   // Take care of the -2pi part
    hf[i][1] = -LAL_TWOPI*h[2*i+1];
  }

  hf[0][0] = 0.0;
  hf[0][1] = 0.0;

  for(i=1; i< N/2; i++)
  {
    if(i>imin && i<imax)
    {
      x = sqrt(Snf[i]);

      // Need to switch real and imaginary parts [ie -i*(x+iy)=-ix+y]

      xx = hf[i][0]; // Real part
      yy = hf[i][1]; // Imaginary part
      hf[i][0] =  yy*(double)i*deltaF/x; // f = i*deltaf
      hf[i][1] = -xx*(double)i*deltaF/x; // Change sign of new imaginary part so it doesn't come out time reversed
    }
    else
    {
      hf[i][0]=0.0;
      hf[i][1]=0.0;
    }
  }
  
  //get DC and Nyquist where FFTW wants them
  hf[N/2][0] = hf[0][1];
  hf[N/2][1] = hf[0][1] = 0.0;

  fftw_execute(reverse);
  
  double norm = sqrt((double)N);

  double t0 = Tobs-2.0;

  for(i=0; i<N; i++)
  {
    t = (double)(i)/(double)(N)*Tobs;
    if(t>=tmin && t<tmax)fprintf(waveout,"%e %e\n", t-t0, ht[i]/norm);
  }

  free(ht);
  fftw_destroy_plan(reverse);
  fftw_free(hf);
  fclose(waveout);
}

void print_frequency_domain_waveforms(char filename[], double *h, int N, double *Snf, double Tobs, int imin, int imax)
{
  int i;
  double x,f;
  FILE *waveout = fopen(filename,"w");

  double *hf = malloc(N*sizeof(double));
  for(i=0; i<N; i++)hf[i]=h[i];

  for(i=imin; i<imax; i++)
  {
    x = sqrt(Snf[i]);
    hf[2*i] /= x;
    hf[2*i+1] /= x;
  }

  for(i=imin; i<imax; i++)
  {
    f = (double)i/Tobs;
    fprintf(waveout,"%e %e %e %e\n", f, hf[2*i], hf[2*i+1], Snf[i]);
  }

  free(hf);

  fclose(waveout);
}

void print_frequency_domain_data(char filename[], double *h, int N, double Tobs, int imin, int imax)
{
  int i,re,im;
  FILE *FDdata = fopen(filename,"w");
  for(i=0; i<N/2; i++)
  {
    re = 2*i;
    im = 2*i+1;

    fprintf(FDdata,"%lg ",i/Tobs);
    if(i<imin || i>imax)
      fprintf(FDdata,"%e %e\n", 0.0, 0.0);
    else
      fprintf(FDdata,"%e %e\n", h[re],h[im]);
  }
  fclose(FDdata);
}

void parse_command_line(struct Data *data, struct Chain *chain, struct Prior *prior, ProcessParamsTable *commandLine)
{
  ProcessParamsTable *ppt = NULL;

  /* Set defaults for model flags */
  //models on by default
  data->cleanModelFlag  = 1;
  data->noiseModelFlag  = 1;
  data->glitchModelFlag = 1;
  data->signalModelFlag = 1;

  //models off by default
  data->fullModelFlag   = 0;
  data->cleanOnlyFlag   = 0;

  //assume the data is analyzable 
  data->loudGlitchFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine,"--srate");
  if(ppt) data->srate = atoi(ppt->value);
  else data->srate = 512;

  ppt = LALInferenceGetProcParamVal(commandLine, "--window");
  if(ppt) data->Twin = atof(ppt->value);
  else data->Twin = 1.0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--srate");
  if(ppt) data->srate = atoi(ppt->value);
  else data->srate = 1024;

  ppt = LALInferenceGetProcParamVal(commandLine, "--Nchain");
  if(ppt) chain->NC = atoi(ppt->value);
  else chain->NC = 20;

  ppt = LALInferenceGetProcParamVal(commandLine, "--Niter");
  if(ppt) chain->count = atoi(ppt->value);
  else chain->count = 4000000;

  ppt = LALInferenceGetProcParamVal(commandLine, "--Ncycle");
  if(ppt) chain->cycle = atoi(ppt->value);
  else chain->cycle = 100;

  ppt = LALInferenceGetProcParamVal(commandLine, "--Nburnin");
  if(ppt) chain->burn = atoi(ppt->value);
  else chain->burn = 50000;
  
  ppt = LALInferenceGetProcParamVal(commandLine, "--maxLogL");
  if(ppt) data->searchFlag = 1;
  else data->searchFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--chainseed");
  if(ppt) gsl_rng_set(chain->seed,atoi(ppt->value));
  else gsl_rng_set(chain->seed,1234);

  ppt = LALInferenceGetProcParamVal(commandLine, "--Dmin");
  if(ppt) data->Dmin = atoi(ppt->value);
  else data->Dmin = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--Dmax");
  if(ppt) data->Dmax = atoi(ppt->value);
  else data->Dmax = 100;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noWaveletPrior");
  if(ppt) data->waveletPriorFlag = 0;
  else data->waveletPriorFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--fixD");
  if(ppt)
  {
    data->rjFlag = 0;
    data->Dmin = atoi(ppt->value);
  }
  else data->rjFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--Qmin");
  if(ppt) data->Qmin = atof(ppt->value);
  else data->Qmin = 0.1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--Qmax");
  if(ppt) data->Qmax = atof(ppt->value);
  else data->Qmax = 40.0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--tempMin");
  if(ppt) chain->tempMin = (double)atof(ppt->value);
  else chain->tempMin = 1.0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--tempSpacing");
  if(ppt) chain->tempStep = (double)atof(ppt->value);
  else chain->tempStep = 1.2;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noAdaptTemperature");
  if(ppt) data->adaptTemperatureFlag = 0;
  else data->adaptTemperatureFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--checkpoint");
  if(ppt) data->checkpointFlag = 1;
  else data->checkpointFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noClean");
  if(ppt) data->cleanModelFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noSignal");
  if(ppt) data->signalModelFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--cleanOnly");
  if(ppt)
  {
    data->cleanOnlyFlag   = 1;
    data->noiseModelFlag  = 0;
    data->glitchModelFlag = 0;
    data->signalModelFlag = 0;
    data->fullModelFlag   = 0;
  }

  ppt = LALInferenceGetProcParamVal(commandLine, "--noiseOnly");
  if(ppt)
  {
    data->noiseModelFlag  = 1;
    data->glitchModelFlag = 0;
    data->signalModelFlag = 0;
    data->fullModelFlag   = 0;
  }

  ppt = LALInferenceGetProcParamVal(commandLine, "--glitchOnly");
  if(ppt)
  {
    data->noiseModelFlag  = 0;
    data->glitchModelFlag = 1;
    data->signalModelFlag = 0;
    data->fullModelFlag   = 0;
  }

  ppt = LALInferenceGetProcParamVal(commandLine, "--signalOnly");
  if(ppt)
  {
    data->noiseModelFlag  = 0;
    data->glitchModelFlag = 0;
    data->signalModelFlag = 1;
    data->fullModelFlag   = 0;
  }

  ppt = LALInferenceGetProcParamVal(commandLine, "--fullOnly");
  if(ppt)
  {
    data->noiseModelFlag  = 0;
    data->glitchModelFlag = 0;
    data->signalModelFlag = 0;
    data->fullModelFlag   = 1;
  }

  ppt = LALInferenceGetProcParamVal(commandLine, "--stochastic");
  if(ppt)
  {
    data->stochasticFlag  = 1;
    data->noiseModelFlag  = 1;
    data->glitchModelFlag = 0;
    data->signalModelFlag = 0;
    data->fullModelFlag   = 0;
  }
  else data->stochasticFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--clusterPrior");
  if(ppt) data->clusterPriorFlag = 1;
  else data->clusterPriorFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--backgroundPrior");
  if(ppt)
  {
    data->backgroundPriorFlag = 1;
    sprintf(prior->bkg_name,"%s",ppt->value);
  }
  else data->backgroundPriorFlag  = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noPolarization");
  if(ppt) data->polarizationFlag = 0;
  else data->polarizationFlag = 1;
  
  ppt = LALInferenceGetProcParamVal(commandLine, "--fixExtrinsicParams");
  if(ppt) data->fixExtrinsicFlag = 1;
  else data->fixExtrinsicFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--fixIntrinsicParams");
  if(ppt) data->fixIntrinsicFlag = 1;
  else data->fixIntrinsicFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noOrientationProposal");
  if(ppt) data->orientationProposalFlag = 0;
  else data->orientationProposalFlag = 1;

  //TODO:  Disabling the extrinsic amplitude updates (severly biasing Q, A)
  ppt = LALInferenceGetProcParamVal(commandLine, "--varyExtrinsicAmplitude");
  if(ppt) data->extrinsicAmplitudeFlag = 1;
  else data->extrinsicAmplitudeFlag = 0;

    ppt = LALInferenceGetProcParamVal(commandLine, "--fixedGeocenterPSD");
    if(ppt) data->geocenterPSDFlag = 0;
    else data->geocenterPSDFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--uniformAmplitudePrior");
  if(ppt) data->amplitudePriorFlag = 0;
  else data->amplitudePriorFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noAmplitudeProposal");
  if(ppt)data->amplitudeProposalFlag = 0;
  else data->amplitudeProposalFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noSignalAmplitudePrior");
  if(ppt)data->signalAmplitudePriorFlag = 0;
  else data->signalAmplitudePriorFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--ampPriorPeak");
  if(ppt) prior->gSNRpeak = (double)atof(ppt->value);
  else prior->gSNRpeak=5.0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--signalPriorPeak");
  if(ppt) prior->sSNRpeak = (double)atof(ppt->value);
  else prior->sSNRpeak=5.0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--dimensionDecayRate");
  if(ppt) prior->Dtau = (double)atof(ppt->value);
  else prior->Dtau=1000.0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noClusterProposal");
  if(ppt) data->clusterProposalFlag = 0;
  else data->clusterProposalFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noLogClusterProposal");
  if(ppt) data->logClusterProposalFlag = 0;
  else data->logClusterProposalFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--clusterWeight");
  if(ppt) data->TFtoProx = 1.0-(double)atof(ppt->value);
  else data->TFtoProx = 0.5;

  ppt = LALInferenceGetProcParamVal(commandLine, "--runName");
  if(ppt) sprintf(data->runName,"%s_",ppt->value);
  else data->runName[0]=0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--outputDir");
  if(ppt) sprintf(data->outputDirectory,"%s/",ppt->value);
  else sprintf(data->outputDirectory,"./");

  ppt = LALInferenceGetProcParamVal(commandLine, "--bayesLine");
  if(ppt) data->bayesLineFlag = 1;
  else data->bayesLineFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--0noise");
  if(ppt) data->noiseSimFlag = 0;
  else data->noiseSimFlag = 1;

  ppt = LALInferenceGetProcParamVal(commandLine, "--prior");
  if(ppt) data->constantLogLFlag = 1;
  else data->constantLogLFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--gnuplot");
  if(ppt) data->gnuplotFlag = 1;
  else data->gnuplotFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--verbose");
  if(ppt) data->verboseFlag = 1;
  else data->verboseFlag = 0;

  ppt = LALInferenceGetProcParamVal(commandLine, "--noSplineEvidence");
  if(ppt) data->splineEvidenceFlag = 0;
  else data->splineEvidenceFlag = 1;

  /*** Cluster prior ***/

  // what we mean by close
  ppt = LALInferenceGetProcParamVal(commandLine, "--clusterAlpha");
  if(ppt) prior->alpha = (double)atof(ppt->value);
  else prior->alpha = 2.0;

  // cluster prior cpen^-K
  ppt = LALInferenceGetProcParamVal(commandLine, "--clusterBeta");
  if(ppt) prior->beta = (double)atof(ppt->value);
  else prior->beta = 4.0;

  /*
   Crediting the Occam penalty for each wavelet if it is in a cluster
   Penalty is D for D degrees of freedom.
   So 5 if we want to cover all the parameters in a wavelet.
   */
  ppt = LALInferenceGetProcParamVal(commandLine, "--clusterGamma");
  if(ppt) prior->gamma = (double)atof(ppt->value);
  else prior->gamma = 0.0;

  /*
   path to files containing normalization for cluster prior
   */
  ppt = LALInferenceGetProcParamVal(commandLine, "--clusterPath");
  if(ppt) sprintf(prior->path,"%s",ppt->value);
  else prior->path[0]=0;

  /*
   have bayeswave_post clean up chains/ and checkpoint/ directories when finished
  */
  ppt = LALInferenceGetProcParamVal(commandLine, "--nuke");
  if(ppt) data->nukeFlag = 1;
  else data->nukeFlag = 0;
    
  /* chirplet basis */
  ppt = LALInferenceGetProcParamVal(commandLine, "--chirplets");
  if(ppt) data->chirpletFlag = 1;
  else data->chirpletFlag = 0;


  /*** Get GMST for the trigger time ***/
  LALStatus status;
  char *chartmp=NULL;
  LIGOTimeGPS GPStrig;

  memset(&status,0,sizeof(status));

  ppt = LALInferenceGetProcParamVal(commandLine,"--trigtime");
  LALStringToGPS(&status,&GPStrig,ppt->value,&chartmp);
  double gmst=(double)XLALGreenwichMeanSiderealTime(&GPStrig);

  //remap gmst back to [0:2pi]
  int intpart;
  double decpart;
  gmst /= LAL_TWOPI;
  intpart = (int)( gmst );
  decpart = gmst - (double)intpart;
  gmst = decpart*LAL_TWOPI;
  data->gmst = gmst;

  /*** Get channel names for each interferometer ***/
  data->channels = malloc(3 * sizeof(char*));
  getChannels(commandLine, data->channels);

  //store command line in data structure for printing later on
  data->commandLine = commandLine;

  //BayesLine requires the cleaning phase
  if(data->bayesLineFlag == 1 && data->cleanModelFlag == 0)
  {
    fprintf(stdout,"\n");
    fprintf(stdout,"************************ WARNING ************************\n");
    fprintf(stdout,"Assuming clean data outside of the analysis window       \n");
    fprintf(stdout,"For reliable results remove --noClean argument           \n");
    fprintf(stdout,"*********************************************************\n");
  }

  //if doing a short run disable spline thermodynamic integration
  if(chain->count < 1000000 && data->splineEvidenceFlag)
  {
    fprintf(stdout,"\n");
    fprintf(stdout,"************************ WARNING ************************\n");
    fprintf(stdout,"Too few iterations for spline integration (%i)\n",chain->count);
    fprintf(stdout,"Reverting to trapezoid integration\n");
    fprintf(stdout,"Request --Niter > 1000000 for spline integration\n");
    fprintf(stdout,"*********************************************************\n");
    data->splineEvidenceFlag = 0;
  }
}

void parse_bayesline_parameters(struct Data *data, struct Model *model, FILE **splinechain, FILE **linechain, double **psd)
{
  int i,ifo;
  int Nspline;
  int Nline;

  int NI = data->NI;
  int N  = data->N;

  double *f =malloc(N/2*sizeof(double));
  double *Sn=malloc(N/2*sizeof(double));

  splineParams *spline    = NULL;
  lorentzianParams *lines = NULL;

  int PSDimin,PSDimax;

  for(i=0; i<N/2; i++) f[i] = (double)(i)/data->Tobs;

  for(ifo=0; ifo<NI; ifo++)
  {

    //Get dimension of spline model
    fscanf(splinechain[ifo],"%i",&Nspline);

    spline = malloc(sizeof(splineParams));
    create_splineParams(spline,Nspline);

    //Store spline points
    for(i=0; i<Nspline; i++) fscanf(splinechain[ifo],"%lg %lg",&spline->points[i],&spline->data[i]);
    PSDimin = (int)(spline->points[0]*data->Tobs);
    PSDimax = (int)(spline->points[Nspline-1]*data->Tobs);

    //Interpolate control points
    /*
     This is tricky:
     - The GSL cubic spline routine throws errors if interpolation
     is outside of control points.
     - Bayesline only fits to frequencies above fmin
     - Some index gymnastics are needed
     */
    for(i=0; i<N/2; i++) model->SnS[ifo][i] = 1.0;

    CubicSplineGSL(Nspline,spline->points,spline->data,PSDimax-PSDimin,f+PSDimin,Sn+PSDimin);

    //Map interpolated points to PSD used later on
    for(i=PSDimin; i<PSDimax; i++) model->Snf[ifo][i]=exp(Sn[i]);

    destroy_splineParams(spline);

    /*
     THE SPECTRAL LINES MODEL
     */


    //Get dimension of line model
    fscanf(linechain[ifo],"%i",&Nline);

    lines = malloc( sizeof(lorentzianParams) );
    create_lorentzianParams(lines, Nline);


    //Store line parameters
    for(i=0; i<Nline; i++)
    {
      fscanf(linechain[ifo],"%lg %lg %lg",&lines->f[i],&lines->A[i],&lines->Q[i]);

      Lorentzian(model->Snf[ifo], data->Tobs, lines, i, data->N);
    }

    destroy_lorentzianParams(lines);
  }

  for(ifo=0; ifo<NI; ifo++)
  {
    for(i=0; i<N/2; i++)
    {
      psd[ifo][i] = model->Snf[ifo][i];
    }
  }

  free(f);
  free(Sn);

}

void parse_glitch_parameters(struct Data *data, struct Model *model, FILE **paramsfile, double **grec)
{
  int ifo;
  int d;
  int i;

  struct Wavelet **glitch = model->glitch;

  for(ifo=0; ifo<data->NI; ifo++)
  {
    //Zero out signal->templates array (holds the linear combination of wavelets)
    for(i=0; i<data->N; i++) glitch[ifo]->templates[i] = 0.0;

    //Get the number of wavelet basis functions for the current sample
    fscanf(paramsfile[ifo], "%i", &glitch[ifo]->size);

    //Get each wavelets parameters and fill up signal structure
    for(d=1; d<=glitch[ifo]->size; d++)
    {
      glitch[ifo]->index[d]=d;
      for(i=0; i<data->NW; i++)
        fscanf(paramsfile[ifo],"%lg ", &glitch[ifo]->intParams[d][i]);

      //Append new wavelet to glitch[ifo]->templates array
      model->wavelet(glitch[ifo]->templates, glitch[ifo]->intParams[d], data->N, 1, data->Tobs);

    }

    //fill hrec with reconstructed waveform
    for(i=0; i<data->N; i++) grec[ifo][i] = glitch[ifo]->templates[i];
  }
}

void parse_signal_parameters(struct Data *data, struct Model *model, FILE **paramsfile, double **hrec)
{
  int ifo;
  int d;
  int i;
  int n;

  //HACKED SIGNAL PARAMETERS
  for(n=0; n<data->Npol; n++)
  {
    struct Wavelet *signal = model->signal[n];
    
    //Zero out signal->templates array (holds the linear combination of wavelets)
    for(i=0; i<data->N; i++) signal->templates[i] = 0.0;
    
    //Get the number of wavelet basis functions for the current sample
    fscanf(paramsfile[n], "%i", &signal->size);
    
    //First in the wavechain file are the extrinsic parameters
    for(i=0; i<NE; i++) fscanf(paramsfile[n],"%lg", &model->extParams[i]);
    
    //Compute network projection coefficients
    computeProjectionCoeffs(data, model->projection, model->extParams, data->fmin, data->fmax);
    
    //Get each wavelets parameters and fill up signal structure
    for(d=1; d<=signal->size; d++)
    {
      signal->index[d]=d;
      for(i=0; i<data->NW; i++) fscanf(paramsfile[n],"%lg ", &signal->intParams[d][i]);
      
      //Append new wavelet to signal->templates array
      model->wavelet(signal->templates, signal->intParams[d], data->N, 1, data->Tobs);
      
    }
  }

  //Combine template waveforms into polarization array
  combinePolarizations(data, model->signal, model->h, model->extParams, model->Npol);

  //Project geocenter waveform onto network
  waveformProject(data, model->projection, model->extParams, model->response, model->h, data->fmin, data->fmax);

  //Get + and x polarizations of hrec
  //compute_reconstructed_plus_and_cross(data, model->projection, model->extParams, hrecPlus, hrecCross, signal->templates, data->fmin, data->fmax);

  //fill hrec with reconstructed waveform
  for(ifo=0; ifo<data->NI; ifo++) for(i=0; i<data->N; i++) {hrec[ifo][i] = model->response[ifo][i];}
}

void dump_time_domain_waveform(struct Data *data, struct Model *model, struct Prior *prior, char root[])
{
  int d;
  int i;
  int ifo;
  char filename[MAXSTRINGSIZE];

  //TODO: HACKED signal pointer
  struct Wavelet *signal = model->signal[0];

  //Zero out signal->templates array (holds the linear combination of wavelets)
  for(i=0; i<data->N; i++) signal->templates[i] = 0.0;

  //Compute network projection coefficients
  computeProjectionCoeffs(data, model->projection, model->extParams, data->fmin, data->fmax);

  //Get each wavelets parameters and fill up signal structure
  for(d=1; d<=signal->size; d++)
  {
    //Append new wavelet to signal->templates array
    model->wavelet(signal->templates, signal->intParams[d], data->N, 1, data->Tobs);

  }

  //Combine template waveforms into polarization array
  combinePolarizations(data, model->signal, model->h, model->extParams, model->Npol);

  //Project geocenter waveform onto network
  waveformProject(data, model->projection, model->extParams, model->response, model->h, data->fmin, data->fmax);

  for(ifo=0; ifo<data->NI; ifo++)
  {
    sprintf(filename,"%s.%i",root,ifo);
    print_time_domain_waveforms(filename, model->response[ifo], data->N, model->Snf[ifo], data->Tobs, data->imin, data->imax, prior->range[0][0], prior->range[0][1]);
  }
}

void export_cleaned_data(struct Data *data, struct Model *model)
{
  int NI = data->NI;
  double Tobs = data->Tobs;
  int i,ifo,imin,imax;

  FILE *psdFile;
  FILE *asdFile;
  FILE *resFile;

  char filename[MAXSTRINGSIZE];

  for(ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"%s%s_fairdraw_res.dat",data->runName,data->ifos[ifo]);
    resFile = fopen(filename,"w");
    sprintf(filename,"%s%s_fairdraw_psd.dat",data->runName,data->ifos[ifo]);
    psdFile = fopen(filename,"w");
    sprintf(filename,"%s%s_fairdraw_asd.dat",data->runName,data->ifos[ifo]);
    asdFile = fopen(filename,"w");

    imin = data->imin;//(int)(floor(data->bayesline[0]->data->flow  * data->bayesline[0]->data->Tobs));
    imax = data->imax;//(int)(floor(data->bayesline[0]->data->fhigh * data->bayesline[0]->data->Tobs));

    //output PSD (padded for ease of use  in LALInferenceReadData())
    fprintf(psdFile, "%.12g %.12g\n",(double)(imin-1)/data->Tobs, model->Snf[ifo][imin]/(Tobs/2.0));
    fprintf(asdFile, "%.12g %.12g\n",(double)(imin-1)/data->Tobs, sqrt(model->Snf[ifo][imin]/(Tobs/2.0)));
    fprintf(resFile, "%.12g %.12g %.12g\n",(double)(imin-1)/data->Tobs,data->s[ifo][2*imin],data->s[ifo][2*imin+1]);
    for(i=imin; i<imax; i++)
    {
      fprintf(psdFile, "%.12g %.12g\n",(double)i/data->Tobs, model->Snf[ifo][i]/(Tobs/2.0));
      fprintf(asdFile, "%.12g %.12g\n",(double)i/data->Tobs, sqrt(model->Snf[ifo][i]/(Tobs/2.0)));
      fprintf(resFile, "%.12g %.12g %.12g\n",(double)i/data->Tobs,data->s[ifo][2*i],data->s[ifo][2*i+1]);
    }
    fprintf(psdFile, "%.12g %.12g\n",(double)imax/data->Tobs, model->Snf[ifo][imax-1]/(Tobs/2.0));
    fprintf(asdFile, "%.12g %.12g\n",(double)imax/data->Tobs, sqrt(model->Snf[ifo][imax-1]/(Tobs/2.0)));
    fprintf(resFile, "%.12g %.12g %.12g\n",(double)imax/data->Tobs,data->s[ifo][2*(imax-1)],data->s[ifo][2*(imax-1)+1]);

    fclose(psdFile);
    fclose(asdFile);
    fclose(resFile);
  }
}

void system_pause()
{
  printf("Press Any Key to Continue\n");
  getchar();
}

