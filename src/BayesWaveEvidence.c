/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#include <time.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "BayesLine.h"
#include "BayesWave.h"
#include "BayesWaveIO.h"
#include "BayesWaveMCMC.h"
#include "BayesWaveMath.h"
#include "BayesWavePrior.h"
#include "BayesWaveModel.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveProposal.h"
#include "BayesWaveEvidence.h"
#include "BayesWaveLikelihood.h"


static void KILL(char* Message)
{
   fprintf(stderr,"\a\n");
   fprintf(stderr,"%s",Message);
   fprintf(stderr,"Terminating the program.\n\n\n");
   exit(1);

   return;
}

void average_log_likelihood_via_direct_downsampling(struct Chain *chain, double *ACL, int nPoints)
{
   int i,j,ic;

   int lag;
   int count;

   int NC      = chain->NC;
   int imax    = nPoints/2;

   double cumACF;
   double ACF;

   double norm;
   double mean;
   double *temp = malloc(sizeof(double)*nPoints);
   double *lagged = malloc(sizeof(double)*nPoints);

   double **logLchain = chain->logLchain;

   // Compute autocorrelation length of each logL chain
   for(ic=0; ic<NC; ic++)
   {

      //temporarily store chain for each temperature
      for(i=0; i<nPoints; i++) temp[i] = logLchain[ic][i];

      //de-trend logL chain
      mean = gsl_stats_mean(temp, 1, nPoints);
      for (i=0; i<nPoints; i++) temp[i] -= mean;
      norm=0.0;
      for(i=0; i<nPoints; i++)norm+=temp[i]*temp[i];


      lag=1;
      ACL[ic] = 1.0;
      ACF     = 1.0;
      cumACF  = 1.0;

      //while (cumACF >= ACL[ic])
      for(lag=0; lag<imax; lag++)
      {
         ACF=0.0;
         for(j=0; j<=nPoints-lag-1; j++) ACF += temp[j]*temp[j+lag]/norm;
         cumACF += 2.0 * ACF;

         ACL[ic] = (REAL8)lag/(REAL8)5;

         if(cumACF >= ACL[ic]) lag=imax;
      }

      /*
       We want 100 effective samples unless the ACL is longer than Npoints/100
       where we will use the full variance of logL as var(<logL>) -- dramatically
       reducing that samples weight in the integral
       For this case we set ACL[ic] = -1 to flag it as bad chain
       */
      if(ACL[ic]<1.0)ACL[ic] = 1.0;

   }

   //convert sumLogL to average logL
   for(ic=0; ic<NC; ic++)
   {
      count=0;
      chain->avgLogLikelihood[ic]=0.0;
      chain->varLogLikelihood[ic]=0.0;
      for(i=0; i<nPoints; i++)
      {
         if(i%(int)ACL[ic]==0 || ACL[ic]<0.0)
         {
            chain->avgLogLikelihood[ic] += logLchain[ic][i];
            chain->varLogLikelihood[ic] += logLchain[ic][i]*logLchain[ic][i];
            count++;
         }
      }

      chain->avgLogLikelihood[ic] /= (double)count;
      chain->varLogLikelihood[ic] /= (double)count;

      //Var[logL] = <logL^2> - <logL>^2
      chain->varLogLikelihood[ic] = chain->varLogLikelihood[ic] - chain->avgLogLikelihood[ic]*chain->avgLogLikelihood[ic];

      //Var[<logL>] = Var[logL]/N **unless N is small (chain is bad) in which case we'll just use Var[logL]
      if(ACL[ic]>0.0) chain->varLogLikelihood[ic] /= (double)count;
   }

   free(temp);
   free(lagged);
}

void average_log_likelihood_via_ACL_downsampling(int M, struct Chain *chain)
{
   int i,j,ic;

   int lag;
   int count;

   int NC      = chain->NC;
   int nPoints = M/2/chain->cycle;
   int imax    = nPoints/2;

   double cumACF;
   double ACL[NC];
   double ACF;

  double **logLchain = chain->logLchain;

   double norm;
   double mean;
   double *temp = malloc(sizeof(double)*nPoints);
   double *lagged = malloc(sizeof(double)*nPoints);

   // Compute autocorrelation length of each logL chain
   for(ic=0; ic<NC; ic++)
   {

      //temporarily store chain for each temperature
      for(i=0; i<nPoints; i++) temp[i] = logLchain[ic][i];

      //de-trend logL chain
      mean = gsl_stats_mean(temp, 1, nPoints);
      for (i=0; i<nPoints; i++) temp[i] -= mean;
      norm=0.0;
      for(i=0; i<nPoints; i++)norm+=temp[i]*temp[i];


      lag=1;
      ACL[ic] = 1.0;
      ACF     = 1.0;
      cumACF  = 1.0;

      while (cumACF >= ACL[ic])
      {
         ACF=0.0;
         for(j=0; j<=nPoints-lag-1; j++) ACF += temp[j]*temp[j+lag]/norm;
         cumACF += 2.0 * ACF;
         lag++;
         ACL[ic] = (REAL8)lag/(REAL8)5;
         if (lag > imax)
         {
            ACL[ic]=nPoints;
            break;
         }
      }
      if(ACL[ic]<1.)ACL[ic]=1.;


      printf("Total number of post burn-in samples for chain %i:  %i\n",ic,nPoints);
      printf("                                  ACL of chain %i:  %i\n",ic,(int)ACL[ic]);
      printf("         Effective number of samples for chain %i:  %i\n",ic,(int)nPoints/(int)ACL[ic]);

      if(nPoints/(int)ACL[ic] < 100)
      {
         printf("**WARNING**:  CHAIN %i CAN NOT BE TRUSTED -- SETTING Neff = 1\n",ic);
         ACL[ic] = nPoints;
      }

   }

   //convert sumLogL to average logL
   for(ic=0; ic<NC; ic++)
   {
      count=0;
      chain->avgLogLikelihood[ic]=0.0;
      chain->varLogLikelihood[ic]=0.0;
      for(i=0; i<nPoints; i++)
      {
         if(i%(int)ACL[ic]==0 || ACL[ic]==nPoints)
         {
            chain->avgLogLikelihood[ic] += logLchain[ic][i];
            chain->varLogLikelihood[ic] += logLchain[ic][i]*logLchain[ic][i];
            count++;
         }
      }

      chain->avgLogLikelihood[ic] /= (double)count;
      chain->varLogLikelihood[ic] /= (double)count;

      //Var[logL] = <logL^2> - <logL>^2
      chain->varLogLikelihood[ic] = chain->varLogLikelihood[ic] - chain->avgLogLikelihood[ic]*chain->avgLogLikelihood[ic];

      if(ACL[ic]<nPoints)chain->varLogLikelihood[ic] /= (double)count;
   }

   free(temp);
   free(lagged);
}


void average_log_likelihood_via_recursion_downsampling(int M, struct Chain *chain)
{
   int i,ic;

   int lag;
   int count;

   int NC      = chain->NC;
   int nPoints = M/2/chain->cycle;
   int imax    = nPoints/2;

   double s;
   double cumACF;
   double ACL[NC];
   double ACF;

  double **logLchain = chain->logLchain;

   double mean=0.0;
   double *temp = malloc(sizeof(double)*nPoints);
   double *lagged = malloc(sizeof(double)*nPoints);

   double norm;
   for(ic=0; ic<NC; ic++)
   {
      norm=0.0;
      for(i=0; i<nPoints; i++)norm+=temp[i]*temp[i];
      imax=nPoints/2;
      cumACF=1.0;
      ACL[ic]=1.0;
      ACF=1.0;
      s=1.0;
      lag=1;

      while (cumACF >= s)
      {
         for(i=0; i<nPoints-lag; i++)
         {
            temp[i]   = logLchain[ic][i];
            lagged[i] = logLchain[ic][i+lag];
         }
         if(lag==1)
         {
            mean = gsl_stats_mean(temp, 1, nPoints-lag);
         }
         for (i=0; i<nPoints-lag; i++)
         {
            temp[i]   -= mean;
            lagged[i] -= mean;
         }

         ACF = gsl_stats_correlation(temp, 1, lagged, 1, nPoints-lag);
         cumACF += 2.0 * ACF;
         lag++;
         s = (REAL8)lag/(REAL8)5;
         if (lag > imax)
         {
            ACL[ic]=nPoints;
            break;
         }
         ACL[ic]=s;
      }

      printf("Total number of post burn-in samples for chain %i:  %i\n",ic,nPoints);
      printf("Effective sample size of chain %i:                  %g\n",ic,1+cumACF);
   }


   /******************************************************************************/
   /*                                                                            */
   /*  Compute (log) evidence via Thermodynamic Integration                      */
   /*                                                                            */
   /******************************************************************************/

   //convert sumLogL to average logL
   for(ic=0; ic<NC; ic++)
   {
      count=0;
      chain->avgLogLikelihood[ic]=0.0;
      chain->varLogLikelihood[ic]=0.0;
      for(i=0; i<nPoints; i++)
      {
         if(i%(int)ACL[ic]==0)
         {
            chain->avgLogLikelihood[ic] += logLchain[ic][i];
            chain->varLogLikelihood[ic] += logLchain[ic][i]*logLchain[ic][i];
            count++;
         }
      }

      printf("number of samples in average = %i\n",count);
      chain->avgLogLikelihood[ic] /= (double)count;
      chain->varLogLikelihood[ic] /= (double)count;

      //Var[logL] = <logL^2> - <logL>^2
      chain->varLogLikelihood[ic] = chain->varLogLikelihood[ic] - chain->avgLogLikelihood[ic]*chain->avgLogLikelihood[ic];

      /*
       We use the variance of the logL chains to get the variance in our
       estimate for the mean

       so Var[<logL>] = Var[logL] / N
       */
      chain->varLogLikelihood[ic] /= (double)count;//((double)M/chain->cycle/4.);//pow( (double)M/chain->cycle/4., 2.0);
      printf("OLD VARIANCE[%i] = %g\n",ic,chain->varLogLikelihood[ic]);

      free(temp);
      free(lagged);

   }
}

void average_log_likelihood_via_ACF(struct Chain *chain)
{
   int i,j,ic;
   int NC = chain->NC;
   int lag;
   int count=0;
   double ACF;
   double mean;
   int nPoints = chain->nPoints;

  double **logLchain = chain->logLchain;

   double *temp = malloc(sizeof(double)*nPoints);
   double norm;

   double correction;// = 0.0;
   for(ic=0; ic<NC; ic++)
   {
      correction = 0.0;
      count=0;
      chain->avgLogLikelihood[ic]=0.0;
      chain->varLogLikelihood[ic]=0.0;
      for(i=0; i<nPoints; i++)
      {
         chain->avgLogLikelihood[ic] += logLchain[ic][i];
         chain->varLogLikelihood[ic] += logLchain[ic][i]*logLchain[ic][i];
         count++;
      }


      for(i=0; i<nPoints; i++) temp[i] = logLchain[ic][i];

      mean = gsl_stats_mean(temp, 1, nPoints);
      for (i=0; i<nPoints; i++) temp[i] -= mean;
      norm=0.0;
      for(i=0; i<nPoints; i++)norm+=temp[i]*temp[i];
      lag=1;
      ACF=1.0;
      for(lag=1; lag<nPoints; lag++)
      {
         ACF=0.0;
         for(j=0; j<=nPoints-lag-1; j++) ACF += temp[j]*temp[j+lag]/norm;

         correction += 2.0*(1.0 - ((double)lag/(double)nPoints))*ACF;
      }

      printf("number of samples in average = %i\n",count);
      chain->avgLogLikelihood[ic] /= (double)count;
      chain->varLogLikelihood[ic] /= (double)count;

      //Var[logL] = <logL^2> - <logL>^2
      chain->varLogLikelihood[ic] = chain->varLogLikelihood[ic] - chain->avgLogLikelihood[ic]*chain->avgLogLikelihood[ic];

      /*
       We use the variance of the logL chains to get the variance in our
       estimate for the mean

       so Var[<logL>] = Var[logL] / N
       */
      chain->varLogLikelihood[ic] /= (double)count;//((double)M/chain->cycle/4.);//pow( (double)M/chain->cycle/4., 2.0);
      chain->varLogLikelihood[ic] *= (1.0 + correction);
      printf("NEW VARIANCE[%i] = %g\n",ic,chain->varLogLikelihood[ic]);

   }

   free(temp);
}

static void splint(double *xa, double *ya, double *y2a, int n, double x, double *y)
{
   //	void nrerror(char error_text[]);
   int klo,khi,k;
   double h,invh,b,a;

   klo=1;
   khi=n;
   while (khi-klo > 1) {
      k=(khi+klo) >> 1;
      if (xa[k] > x) khi=k;
      else klo=k;
      //printf("%d %f\n", k, xa[k]);
   }
   h=xa[khi]-xa[klo];
   invh=1.0/h;
   //printf("%f %d %f %d %f\n", x, klo, xa[klo], khi, xa[khi]);
   //	if (h == 0.0) nrerror("Bad xa input to routine splint");
   a=(xa[khi]-x)*invh;
   b=(x-xa[klo])*invh;
   *y=a*ya[klo]+b*ya[khi]+((a*a*a-a)*y2a[klo]+(b*b*b-b)*y2a[khi])*(h*h)/6.0;
}

#define NRANSI
static void spline(double *x, double *y, int n, double yp1, double ypn, double *y2)
{
   int i,k;
   double p,qn,sig,un;
   double *u;

   u=double_vector(n-1);
   if (yp1 > 0.99e30)
      y2[1]=u[1]=0.0;
   else {
      y2[1] = -0.5;
      u[1]=(3.0/(x[2]-x[1]))*((y[2]-y[1])/(x[2]-x[1])-yp1);
   }
   for (i=2;i<=n-1;i++) {
      sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
      p=sig*y2[i-1]+2.0;
      y2[i]=(sig-1.0)/p;
      u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
      /*if(i==n-1)printf("%i y[i+1] = %.12g y[i] = %.12g x[i+1] = %.12g x[i] = %.12g u[i-1] = %.12g u[i] = %.12g sig = %.12g \n",i, y[i+1], y[i], x[i+1], x[i], u[i-1], u[i], sig);*/
      u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
      /*if(i==n-1) printf("x[i+1]-x[i-1] = %.12g \n", x[i+1]-x[i-1]);*/
   }
   if (ypn > 0.99e30)	qn=un=0.0;

   else {
      qn=0.5;
      un=(3.0/(x[n]-x[n-1]))*(ypn-(y[n]-y[n-1])/(x[n]-x[n-1]));
   }
   y2[n]=(un-qn*u[n-1])/(qn*y2[n-1]+1.0);
   //printf("qn = %.12g  y2[n-1] = %.12g  u[n-1] = %.12g\n", qn, y2[n-1], u[n-1]);

   for (k=n-1;k>0;k--) y2[k]=y2[k]*y2[k+1]+u[k];

   free_double_vector(u);
}
#undef NRANSI

void SplineThermodynamicIntegration(double *temperature, double *avgLogLikelihood, double *varLogLikelihood, int NC, double *logEvidence, double *varLogEvidence)
{

   int N, Nx, Ny, ND, iendN, iendND;
   int mc, i, j, ii, test, scnt;
   int acc;
   int fixedD;
   double logLx, logLy;
   double logpx, logpy=0.0, logpxtmp, logpytmp;
   double *ref, *sprd, *datax, *datay;
   int *activex, *activey;
   double *counts;

   double alpha, H, av;
   double model;

   double max, min;

   double x, y, y3, y4, x3, x4, *sigma;
   double q, secd;

   double trap;
   double *sdatay, *sderiv, *sgm;
   double *spoints, *sdatax, *tdata, *tpoints;

   double ep, invep2;
   double avr=0, var;


   N = 10000000;  // number of MCMC steps
   iendN = N/2;

   ep = 0.0001;
   invep2 = 1./(ep*ep);

   ND = NC;
   iendND = ND+1;


   datax = double_vector(ND-1);
   datay = double_vector(ND-1);
   sigma = double_vector(ND-1);
   counts = double_vector(ND);


  const gsl_rng_type *T = gsl_rng_default;
  gsl_rng *seed = gsl_rng_alloc(T);
  gsl_rng_env_setup();
  gsl_rng_set (seed, time(0));

   // Read in  Data
   for(j=0; j< ND; j++)
   {
      i = ND-j-1;
      datax[i] = -log(temperature[j]);
      datay[i] = avgLogLikelihood[j];
      sigma[i] = sqrt(varLogLikelihood[j]);
   }


   max = datax[ND-1];
   min = datax[0];

   ref = double_vector(ND);
   sprd = double_vector(ND);
   spoints = double_vector(ND);
   tdata = double_vector(ND);
   tpoints = double_vector(ND);
   sdatax = double_vector(ND);
   sdatay = double_vector(ND);
   sderiv = double_vector(ND);
   sgm = double_vector(ND);

   activex = int_vector(ND);
   activey = int_vector(ND);
  
   for(i=1; i<iendND; i++)
   {
      activex[i] = 1;
      activey[i] = 1;
   }

   // copy over the input data
   for(i=1; i<iendND; i++)
   {
      sdatax[i]= datay[i-1];
      spoints[i] = datax[i-1];
      sderiv[i] = 0.0;
      ref[i] = datay[i-1];
      sprd[i] = 100.0+0.5*fabs(ref[i]);
      //printf("%d %f %f\n", i, ref[i], sprd[i]);
   }

   for(i=1; i< ND; i++)
   {
      sgm[i] = 10.0+ fabs((sdatax[i+1]-sdatax[i])/pow((spoints[i+1]-spoints[i]),2.0));
   }
   sgm[ND] = sgm[ND-1];

   spline(spoints, sdatax, ND, 1.0e31, 1.0e31, sderiv);

   Nx = Ny = ND;

   // set this flag to 1 if you want to do a fixed dimension run
   fixedD = 0;

   for(i=1; i< iendND; i++) counts[i] = 0.0;


   // compute the initial log likelihood
   av = 0.0;
   for(j=0; j< ND; j++)
   {
      splint(spoints, sdatax, sderiv, Nx, datax[j], &model);
      y = (datay[j]-model)/sigma[j];
      av -= y*y;
   }
   logLx = 0.5*av;


   logpx = 0.0;
   for(i=2; i< ND; i++)
   {
      x = 0.5*(datax[i]+datax[i-1]);
      splint(spoints, sdatax, sderiv, Nx, x, &y);
      x3 = x - ep;
      splint(spoints, sdatax, sderiv, Nx, x3, &y3);
      x4 = x + ep;
      splint(spoints, sdatax, sderiv, Nx, x4, &y4);
      secd = (y3+y4-2.0*y)*invep2;
      logpxtmp = secd/sgm[i];
      logpx -= logpxtmp*logpxtmp;
   }
   logpx = 0.5*logpx;

   acc = 1;
   av = 0.0;
   var = 0.0;
   scnt = 0;

   // start the RJMCMC
   for(mc=0; mc< N; mc++)
   {

      test = 0;

      for(i=1; i< iendND; i++)
      {
         sdatay[i] = sdatax[i];
         activey[i] = activex[i];
      }

      alpha = uniform_draw(seed);

      if(fixedD == 1) q = 10.0;
      else q = 0.5;

      if(alpha > q)   // propose a dimension change
      {

         alpha = uniform_draw(seed);

         if(alpha < 0.5)
         {
            Ny = Nx + 1;  // Therefore Ny > Nx
            if(Ny > 0 && Ny < ND)
            {
               do
               {
                  i = 1 + (int)(uniform_draw(seed)*(double)(ND)); // pick one to add
               } while(activex[i] == 1);  // can't add if already in use
               activey[i] = 1;

               sdatay[i] = ref[i]+sprd[i]*(1.0-2.0*uniform_draw(seed));
            }
            else
            {
               test = 1;
            }
         }
         else
         {
            Ny = Nx - 1; //Therefore Ny < Nx --> propose a kill
            if(Ny > 1 && Ny < iendND)
            {
               do
               {
                  i = 1 + (int)(uniform_draw(seed)*(double)(ND)); // pick one to kill
               } while(activex[i] == 0);  // can't kill it if already dead
               activey[i] = 0;
            }
            else
            {
               test = 1;
            }
         }
      }
      else     // within dimension update
      {

         Ny = Nx;

         alpha = uniform_draw(seed);

         // variety of jump sizes
         if(alpha > 0.8)
         {
            for(ii=1; ii<iendND; ii++) sdatay[ii] += sprd[ii]*1.0e-1*gaussian_draw(seed);
         }
         else if (alpha > 0.5)
         {
            for(ii=1; ii<iendND; ii++) sdatay[ii] += sprd[ii]*1.0e-2*gaussian_draw(seed);
         }
         else if (alpha > 0.3)
         {
            for(ii=1; ii<iendND; ii++) sdatay[ii] += sprd[ii]*1.0e-3*gaussian_draw(seed);
         }
         else
         {
            for(ii=1; ii<iendND; ii++) sdatay[ii] += sprd[ii]*1.0e-4*gaussian_draw(seed);
         }

      }

      // check that proposed values are within the prior range
      if(Ny > 0 && Ny < iendND)
      {
         for(ii=1; ii<iendND; ii++) // check the active points are in range
         {
            if(activey[ii] == 1)
            {
               if(sdatay[ii] > ref[ii]+sprd[ii]) test = 1;
               if(sdatay[ii] < ref[ii]-sprd[ii]) test = 1;
            }
         }
      }
      else
      {
         test = 1;
      }

      logLy = 0.0;

      if(test == 0)
      {

         i = 0;
         for(ii=1; ii<iendND; ii++)
         {
            if(activey[ii] == 1)  // only use active points
            {
               i++;
               tpoints[i] = spoints[ii];
               tdata[i] = sdatay[ii];
            }
         }


         spline(tpoints, tdata, Ny, 1.0e31, 1.0e31, sderiv);
         av = 0.0;
         for(j=0; j< ND; j++)
         {
            splint(tpoints, tdata, sderiv, Ny, datax[j], &model);
            y = (datay[j]-model)/sigma[j];
            av -= y*y;
         }
         logLy = 0.5*av;

         logpy = 0.0;
         for(i=2; i< ND; i++)
         {
            x = 0.5*(datax[i]+datax[i-1]);
            splint(tpoints, tdata, sderiv, Ny, x, &y);
            x3 = x - ep;
            splint(tpoints, tdata, sderiv, Ny, x3, &y3);
            x4 = x + ep;
            splint(tpoints, tdata, sderiv, Ny, x4, &y4);
            secd = (y3+y4-2.0*y)*invep2;
            logpytmp = secd/sgm[i];
            logpy -= logpytmp*logpytmp;
         }
         logpy = 0.5*logpy;
      }

      H = logLy-logLx +logpy - logpx;
      alpha = log(uniform_draw(seed));

      if((H > alpha) && (test==0))
      {
         acc++;
         logLx = logLy;
         logpx = logpy;
         Nx = Ny;
         for(i=1; i<iendND; i++)
         {
            sdatax[i] = sdatay[i];
            activex[i] = activey[i];
         }

      }

      // This gives us the data for Bayes factors. Don't start collecting it until after burn-in
      if(mc > iendN) counts[Nx] += 1.0;

      if(mc%1000 == 0)
      {

         i = 0;
         for(ii=1; ii<iendND; ii++)
         {
            if(activex[ii] == 1)  // only use active points
            {
               i++;
               tpoints[i] = spoints[ii];
               tdata[i] = sdatax[ii];
            }
         }


         spline(tpoints, tdata, Nx, 1.0e31, 1.0e31, sderiv);

         trap = 0.0;
         x3 = min;
         splint(tpoints, tdata, sderiv, Nx, x3, &y3);
         for(i=1; i< 1001; i++)
         {
            x = x3;
            y = y3;
            x3 = min + 0.001*(max-min)*(double)(i);
            splint(tpoints, tdata, sderiv, Nx, x3, &y3);
            trap += 0.5*(x3-x)*(exp(x3)*y3+exp(x)*y);
         }

         scnt++;
         avr += trap;
         var += trap*trap;

      }

   }

   x = avr/(double)(scnt);
   y = var/(double)(scnt);

   *logEvidence = x;
   *varLogEvidence = y-x*x;

   free_double_vector(datax);
   free_double_vector(datay);
   free_double_vector(sigma);
   free_double_vector(counts);

   free_double_vector(ref);
   free_double_vector(sprd);
   free_double_vector(spoints);
   free_double_vector(tdata);
   free_double_vector(tpoints);
   free_double_vector(sdatax);
   free_double_vector(sdatay);
   free_double_vector(sderiv);
   free_double_vector(sgm);

   free_int_vector(activex);
   free_int_vector(activey);

  gsl_rng_free(seed);
}


void bootcorrposdef(double **data, int NC, int N, double **cmat, double *mu, gsl_rng *seed)
{
   double av;
   double x, y, shrink;
   int i, j, k, cmin, icm, cmax;
   int ii, jj, kk, nn;
   int *cyc;
   int cycles;
   int so, sc, cp;
   int b, bs, bsteps;
   int *cnts;
   double **fmean;
   double *var, *mx;

   // This code uses the "threshold bootstrap" method to compute the covariance matrix
   // See http://www.sciencedirect.com/science/article/pii/S0377221700002095 and
   /* @inproceedings{Kim:1993:TBN:256563.256697,
    author = {Kim, Y. B. and Willemain, T. R. and Haddock, J. and Runger, G. C.},
    title = {The Threshold Bootstrap: A New Approach to Simulation Output Analysis},
    booktitle = {Proceedings of the 25th Conference on Winter Simulation},
    series = {WSC '93},
    year = {1993},
    isbn = {0-7803-1381-X},
    location = {Los Angeles, California, USA},
    pages = {498--502},
    numpages = {5},
    url = {http://doi.acm.org/10.1145/256563.256697},
    doi = {10.1145/256563.256697},
    acmid = {256697},
    publisher = {ACM},
    address = {New York, NY, USA},
    } */

   // The "shrinkage" method described here is uses to ensure the covariance matrices are
   // postive definite and well balanced https://www.stat.wisc.edu/courses/st992-newton/smmb/files/expression/shrinkcov2005.pdf

   bsteps = 100000;

   var = double_vector(NC);
   cnts = int_vector(NC);

   for(ii=0; ii< NC; ii++)
   {
      mu[ii] = 0.0;
      var[ii] = 0.0;
      for(i=0; i< N; i++)
      {
         mu[ii] += data[ii][i];
         var[ii] += data[ii][i]*data[ii][i];
      }
      mu[ii] /= (double)(N);
      var[ii] /= (double)(N);
      var[ii] -= mu[ii]*mu[ii];
      var[ii] = sqrt(var[ii]);
   }

   b = 8;
   // if b > 1, then what I'm calling cycles are actually chunks, made up of b cycles

   icm = 0;
   do
   {
      kk = 0;
      nn = 0;
      // cycle lengths for individual chains
      cmin = 100000;
      cmax = 0;
      for(ii=0; ii< NC; ii++)
      {
         cycles = 0;
         so = -1;
         if((data[ii][0]-mu[ii]) > 0.0) so = 1;
         cp = 0;

         for(i=0; i< N; i++)
         {
            sc = -1;
            if((data[ii][i]-mu[ii]) > 0.0) sc = 1;
            if(sc != so) cp++;   // count the change points
            so = sc;
            if(cp == 2*b)
            {
               cycles++;
               cp = 0;
            }
         }

         cnts[ii] = cycles;
         if(cycles < cmin)
         {
            cmin = cycles;
            icm = ii;
         }
         if(cycles > cmax)
         {
            cmax = cycles;
         }

         kk += cycles;
         nn += cycles*cycles;

      }

      // looking for between 100 and 1000 chunks
      // This assumes that we have a decent amount of data to begin with

      if(cmin <  100) b /= 2;
      if(cmin > 1000) b *= 2;

      if(b < 2)
      {
         b = 2;
         cmin = 500;
      }


   } while(cmin < 100 || cmin > 1000);

   // record the change point for each cycle
   // if cyc = i, then one cycle ends at i and the next starts at i+1
   cyc = int_vector(N/2);

   cyc[0] = -1;



   av = 0.0;

   for(i=0; i< N; i++)
   {
      av += data[icm][i];    // correlation length determined by most corelated
   }
   av /= (double)(N);


   cycles = 0;
   so = -1;
   if((data[icm][0]-av) > 0.0) so = 1;

   cp = 0;

   for(i=0; i< N; i++)
   {
      sc = -1;
      if((data[icm][i]-av) > 0.0) sc = 1;
      if(sc != so) cp++;   // count the change points
      so = sc;
      if(cp == 2*b)
      {
         cycles++;
         cyc[cycles] = i-1;
         cp = 0;
      }
   }

   // create bootstrap data sets

   fmean = double_matrix(NC,bsteps);  // averages for each bootstrap at each temperature

   for(bs=0; bs< bsteps; bs++)
   {
      for(ii=0; ii< NC; ii++) fmean[ii][bs] = 0.0;
   }

   for(bs=0; bs< bsteps; bs++)
   {

      k = -1;
      do
      {
         i = (double)(cycles)*uniform_draw(seed)+1;  // pick a chunk
         for(j=(cyc[i-1]+1); j<= cyc[i]; j++)
         {
            k++;
            if(k < N)
            {
               for(ii=0; ii< NC; ii++) fmean[ii][bs] += data[ii][j];
            }
         }

      }while(k < N);

      for(ii=0; ii< NC; ii++) fmean[ii][bs] /= (double)(N);

   }

   // means and variances of the bootstraps
   mx = double_vector(NC);

   for(ii=0; ii< NC; ii++)
   {
      mx[ii] = 0.0;
      var[ii] = 0.0;
      for(bs=0; bs< bsteps; bs++)
      {
         mx[ii] += fmean[ii][bs];
         var[ii] += fmean[ii][bs]*fmean[ii][bs];
      }
      mx[ii] /= (double)(bsteps);
      var[ii] /= (double)(bsteps);
      var[ii] -= mx[ii]*mx[ii];
      var[ii] = sqrt(var[ii]);
      //standardize the data
      for(bs=0; bs< bsteps; bs++)
      {
         fmean[ii][bs] = (fmean[ii][bs]-mx[ii])/var[ii];
      }
   }

   double **rmat, **rvar;

   rmat = double_matrix(NC,NC);
   rvar = double_matrix(NC,NC);

   rvar[0][0] = 0.0;

   // next we compute the correlation r_ij and its variance
   for(ii=0; ii< NC; ii++)
   {
      for(jj=0; jj< NC; jj++)
      {
         rmat[ii][jj] = 0.0;
         for(bs=0; bs< bsteps; bs++)	rmat[ii][jj] += fmean[ii][bs]*fmean[jj][bs];
         rmat[ii][jj] /= (double)(bsteps);
      }
   }

   /*for(ii=0; ii< 10; ii++)
    {
    for(jj=0; jj< 10; jj++)
    {
    printf("%f ", rmat[ii][jj]);
    }
    printf("\n");
    }*/


   for(ii=0; ii< NC; ii++)
   {
      for(jj=0; jj< NC; jj++)
      {
         rvar[ii][jj] = 0.0;
         for(bs=0; bs< bsteps; bs++)	rvar[ii][jj] += pow((fmean[ii][bs]*fmean[jj][bs]-rmat[ii][jj]),2.0);
         rvar[ii][jj] *= (double)(bsteps)/pow((double)(bsteps-1),3.0);
      }
   }

   x = 0.0;
   y = 0.0;
   for(ii=0; ii< NC; ii++)
   {
      for(jj=0; jj< NC; jj++)
      {
         if(ii != jj)
         {
            x += rvar[ii][jj];
            y += rmat[ii][jj]*rmat[ii][jj];
         }
      }
   }

   shrink = 1.0-x/y;
   if(shrink < 0.0) shrink = 0.0;
   if(shrink > 1.0) shrink = 1.0;


   for(ii=0; ii< NC; ii++)
   {
      for(jj=0; jj< NC; jj++)
      {
         if(ii == jj)
         {
            cmat[ii][jj] = var[ii]*var[jj];
         }
         else
         {
            cmat[ii][jj] = shrink*var[ii]*var[jj]*rmat[ii][jj];
         }
      }
   }

   free_double_matrix(rmat,NC);
   free_double_matrix(rvar,NC);
   free_int_vector(cyc);
   free_int_vector(cnts);
   free_double_matrix(fmean,NC);
   free_double_vector(var);
   free_double_vector(mx);


}

void CovarianceThermodynamicIntegration(double *temperature, double **loglikelihood, int M, int NC, char modelname[], double *logEvidence, double *varLogEvidence)
{


   int N, Nx, Ny, ND, NSP;
   int mc, i, j, ii, test, scnt, iendN, iendND, iendNSP;
   int acc;
   int fixedD;
   double logLx, logLy;
   double logpx, logpy=0.0, logpxtmp, logpytmp;
   double *ref, *sprd, *datax, *datay, *dataxExp;
   int *activex, *activey;
   int NL;
   double *counts;
   double alpha, H, av;
   double model;
   double max, min;
   double x, y, y3, y4, x3, x4, dx, *sigma;
   double q;
   double trap,fird,secd;
   double *sdatay, *sderiv, *sgm, *srm;
   double *spoints, *sdatax, *tdata, *tpoints;
   double ep, invep2, inv2ep;
   double avr=0.0, var;
   double *mdl;
   double **cov,**chains;

   char filename[100];
   FILE *outfile;

   N = 1000000;  // number of MCMC steps
   iendN = N/2;

   // sets the degree of smoothing for the spline
   double smooth = 8.0;

   ep = 0.0001;
   invep2 = 1./(ep*ep);
   inv2ep = 0.5/ep;


   ND = NC;
   NL = M;

   // maximum number of spline points - we initially overcover
   NSP = 2*ND-1;
   iendND = ND+1;
   iendNSP = NSP+1;

   datax    = double_vector(ND-1);
   dataxExp = double_vector(ND-1);
   datay    = double_vector(ND-1);
   counts   = double_vector(NSP);
   sigma    = double_vector(ND-1);
   mdl      = double_vector(ND-1);
   chains   = double_matrix(ND-1,NL-1);
   cov      = double_matrix(ND-1,ND-1);
  
  const gsl_rng_type *T = gsl_rng_default;
  gsl_rng *seed = gsl_rng_alloc(T);
  gsl_rng_env_setup();
  gsl_rng_set (seed, time(0));

   FILE *temp = fopen("likelihood.dat","w");

   fprintf(temp,"0 ");
   for(j=0; j< ND; j++)
   {
      datax[ND-1-j] = -log(temperature[j]);
      fprintf(temp,"%lg ",1./temperature[j]);
   }fprintf(temp,"\n");

   for(j=0; j< NL; j++)
   {
      fprintf(temp,"%i ",j);
      for(i=0; i< ND; i++)
      {
         chains[ND-1-i][j] = loglikelihood[i][j];
         fprintf(temp,"%lg ",loglikelihood[i][j]);
      }
      fprintf(temp,"\n");
   }
   fclose(temp);

   bootcorrposdef(chains, ND, NL, cov, datay, seed);


   /******************************************/
   /*                                        */
   /*  Linear algebra gymnastics with the    */
   /*  correlation matrix                    */
   /*                                        */
   /******************************************/

   /* allocate memory for GSL linear algebra */

   gsl_matrix *matrix  = gsl_matrix_alloc (ND, ND);
   gsl_matrix *inverse = gsl_matrix_alloc (ND, ND);

   gsl_vector *eval = gsl_vector_alloc (ND);
   gsl_matrix *evec = gsl_matrix_alloc (ND, ND);

   gsl_permutation *perm = gsl_permutation_alloc(ND);

   gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc (ND);


   // get eigenvalues and eigenvectors of correlation matrix (matrix is destroyed)
   for (i = 0; i < ND; i++)
   {
      sigma[i] = sqrt(cov[i][i]);
      for (j = 0; j < ND; j++)
      {
         gsl_matrix_set(matrix,i,j,cov[i][j]);
      }
   }

   // the eigenstuff calculator
   gsl_eigen_symmv(matrix, eval, evec, w);

   /*
    if the correlation matrix is up to snuff
    calculate its inverse
    */
   for(i=0; i<ND; i++)
   {
      if(gsl_vector_get(eval,i) < 0.0)
      {
         KILL("Correlation matrix not positive definite\n");
      }
      else
      {
         for(j=0; j<ND; j++) gsl_matrix_set(matrix,i,j,cov[i][j]);
      }
   }

   // Make LU decomposition of matrix
   gsl_linalg_LU_decomp (matrix, perm, &i);

   // Invert the matrix
   gsl_linalg_LU_invert (matrix, perm, inverse);

   // print out data with error bars

   sprintf(filename,"%s_bootstrap_evidence.dat",modelname);
   outfile = fopen(filename,"w");
   for(j=0; j< ND; j++)
   {
      fprintf(outfile,"%f %f %f\n", datax[j], datay[j], sigma[j]);
   }
   fclose(outfile);


   // compute simple trapezoid integrand and error
   trap = 0.0;
   for(j=0; j< ND; j++) dataxExp[j] = exp(datax[j]);
   for(j=1; j< ND; j++)
   {
      trap += 0.5*(datax[j]-datax[j-1])*(dataxExp[j]*datay[j]+dataxExp[j-1]*datay[j-1]);
   }

   double b1, b0;
   double s1, s0;

   x = 0.0;
   for(j=1; j< (ND-1); j++)
   {

      if(j==1)
      {
         b0 = 0.0;
      }
      else
      {
         b0 = (datax[j-1]-datax[j-2]);
      }
      b1 = (datax[j]-datax[j-1]);
      s0 = sigma[j-1]*dataxExp[j-1];
      s1 = sigma[j]*dataxExp[j];
      s0 = s0*s0;
      s1 = s1*s1;

      x += 0.25*((s1+s0)*b1*b1+2.0*s0*b1*b0);

   }



   max = datax[ND-1];
   min = datax[0];

   /* Allocate memory for spline fit*/
   ref = double_vector(NSP);
   sprd = double_vector(NSP);
   spoints = double_vector(NSP);
   tdata = double_vector(NSP);
   tpoints = double_vector(NSP);
   sdatax = double_vector(NSP);
   sdatay = double_vector(NSP);
   sderiv = double_vector(NSP);
   sgm = double_vector(NSP);
   srm = double_vector(NSP);

   activex = int_vector(NSP);
   activey = int_vector(NSP);
  
   for(i=1; i<iendNSP; i++)
   {
      activex[i] = 1;
      activey[i] = 1;
   }

   // copy over the input data
   for(i=1; i<iendND; i++)
   {
      sdatax[2*i-1]= datay[i-1];
      spoints[2*i-1] = datax[i-1];
      sderiv[2*i-1] = 0.0;
      ref[2*i-1] = datay[i-1];
      sprd[2*i-1] = 2.0*sigma[i-1];
      if(2*i < iendNSP)
      {
         sdatax[2*i]= 0.5*(datay[i-1]+datay[i]);
         spoints[2*i] = 0.5*(datax[i-1]+datax[i]);
         sderiv[2*i] = 0.0;
         ref[2*i] = sdatax[2*i];
         sprd[2*i] = sigma[i-1]+sigma[i];
      }
   }

   spline(spoints, sdatax, NSP, 1.0e31, 1.0e31, sderiv);

   Nx = Ny = NSP;

   // set this flag to 1 if you want to do a fixed dimension run
   fixedD = 0;

   for(i=1; i< iendNSP; i++) counts[i] = 0.0;


   // compute the initial log likelihood


   for(j=0; j< ND; j++)
   {
      splint(spoints, sdatax, sderiv, Nx, datax[j], &model);
      mdl[j] = model;
   }

   trap = 0.0;
   x3 = min;
   splint(spoints, sdatax, sderiv, Nx, x3, &y3);
   for(i=1; i< 1001; i++)
   {
      x = x3;
      y = y3;
      x3 = min + (max-min)*(double)(i)/1000.0;
      splint(spoints, sdatax, sderiv, Nx, x3, &y3);
      trap += 0.5*(x3-x)*(exp(x3)*y3+exp(x)*y);
   }

   /* Initialize Likelhood */
   av = 0.0;
   for (i = 0; i < ND; i++)
   {
      for (j = 0; j < ND; j++)
      {
         av += gsl_matrix_get(inverse,i,j)*(datay[i]-mdl[i])*(datay[j]-mdl[j]);
      }
   }
   logLx = -0.5*av;

   /* Initialize Prior */
   logpx = 0.0;
   for(i=2; i< NSP; i++)
   {
      x = spoints[i];
      dx = spoints[i]-spoints[i-1];
      splint(spoints, sdatax, sderiv, Nx, x, &y);
      x3 = x - ep;
      splint(spoints, sdatax, sderiv, Nx, x3, &y3);
      x4 = x + ep;
      splint(spoints, sdatax, sderiv, Nx, x4, &y4);
      secd = (y3+y4-2.0*y)*invep2;
      fird = (y4-y3)*inv2ep;
      logpxtmp = secd*dx/fird;
      logpx -= logpxtmp*logpxtmp;
   }
   logpx = smooth*logpx;

   acc = 1;
   av = 0.0;
   var = 0.0;
   scnt = 0;

   // start the RJMCMC
   for(mc=0; mc< N; mc++)
   {

      test = 0;

      for(i=1; i<iendNSP; i++)
      {
         sdatay[i] = sdatax[i];
         activey[i] = activex[i];
      }

      alpha = uniform_draw(seed);

      q = 0.5;
      if(fixedD == 1) q = 10.0;

      if(alpha > q)   // propose a dimension change
      {

         alpha = uniform_draw(seed);

         if(alpha < 0.5)
         {
            Ny = Nx + 1;
         }
         else
         {
            Ny = Nx - 1;
         }

         if(Ny < Nx) // propose a kill
         {
            if(Ny > 1 && Ny < iendNSP)
            {

               do
               {
                  i = 1 + (int)(uniform_draw(seed)*(double)(NSP)); // pick one to kill
               } while(activex[i] == 0);  // can't kill it if already dead
               activey[i] = 0;
            }
            else
            {
               test = 1;
            }
         }
         else
         {
            if(Ny > 0 && Ny < NSP)
            {

               do
               {
                  i = 1 + (int)(uniform_draw(seed)*(double)(NSP)); // pick one to add
               } while(activex[i] == 1);  // can't add if already in use
               activey[i] = 1;

               sdatay[i] = ref[i]+10.0*sprd[i]*(1.0-2.0*uniform_draw(seed));

            }
            else
            {
               test = 1;
            }


         }


      }
      else     // within dimension update
      {

         Ny = Nx;

         alpha = uniform_draw(seed);

         for(ii=1; ii<iendNSP; ii++)
         {
            // variety of jump sizes
            if(alpha > 0.8)
            {
               sdatay[ii] += sprd[ii]*gaussian_draw(seed);
            }
            else if (alpha > 0.5)
            {
               sdatay[ii] += sprd[ii]*1.0e-1*gaussian_draw(seed);
            }
            else if (alpha > 0.2)
            {
               sdatay[ii] += sprd[ii]*1.0e-2*gaussian_draw(seed);
            }
            else
            {
               sdatay[ii] += sprd[ii]*1.0e-3*gaussian_draw(seed);
            }

         }
      }

      // check that proposed values are within the prior range
      if(Ny > 0 && Ny < iendNSP)
      {
         for(ii=1; ii< iendNSP; ii++) // check the active points are in range
         {
            if(activey[ii] == 1)
            {
               if(sdatay[ii] > ref[ii]+10.0*sprd[ii]) test = 1;
               if(sdatay[ii] < ref[ii]-10.0*sprd[ii]) test = 1;
            }
         }
      }
      else
      {
         test = 1;
      }


      logLy = 0.0;

      if(test == 0)
      {
         i = 0;
         for(ii=1; ii< iendNSP; ii++)
         {
            if(activey[ii] == 1)  // only use active points
            {
               i++;
               tpoints[i] = spoints[ii];
               tdata[i] = sdatay[ii];
            }
         }


         spline(tpoints, tdata, Ny, 1.0e31, 1.0e31, sderiv);
         for(j=0; j< ND; j++)
         {
            splint(tpoints, tdata, sderiv, Ny, datax[j], &model);
            mdl[j] = model;
         }
         av = 0.0;
         for (i = 0; i < ND; i++)
         {
            for (j = 0; j < ND; j++)
            {
               av += gsl_matrix_get(inverse,i,j)*(datay[i]-mdl[i])*(datay[j]-mdl[j]);

            }
         }
         logLy = -0.5*av;

         logpy = 0.0;
         for(i=2; i< NSP; i++)
         {
            x = spoints[i];
            dx = spoints[i]-spoints[i-1];
            splint(tpoints, tdata, sderiv, Ny, x, &y);
            x3 = x - ep;
            splint(tpoints, tdata, sderiv, Ny, x3, &y3);
            x4 = x + ep;
            splint(tpoints, tdata, sderiv, Ny, x4, &y4);
            secd = (y3+y4-2.0*y)*invep2;
            fird = (y4-y3)*inv2ep;
            logpytmp = secd*dx/fird;
            logpy -= logpytmp*logpytmp;
         }
         logpy = smooth*logpy;
      }

      H = logLy-logLx +logpy - logpx;
      // H = logLy-logLx;  // without the smoothing prior
      alpha = log(uniform_draw(seed));

      if((H > alpha) && (test==0))
      {
         acc++;
         logLx = logLy;
         logpx = logpy;
         Nx = Ny;
         for(i=1; i<iendNSP; i++)
         {
            sdatax[i] = sdatay[i];
            activex[i] = activey[i];
         }

      }

      // This gives us the data for Bayes factors. Don't start collecting it until after burn-in
      if(mc > iendN) counts[Nx] += 1.0;

      if(mc%1000 == 0)
      {

         i = 0;
         for(ii=1; ii<iendNSP; ii++)
         {
            if(activex[ii] == 1)  // only use active points
            {
               i++;
               tpoints[i] = spoints[ii];
               tdata[i] = sdatax[ii];
            }
         }


         spline(tpoints, tdata, Nx, 1.0e31, 1.0e31, sderiv);

         trap = 0.0;
         x3 = min;
         splint(tpoints, tdata, sderiv, Nx, x3, &y3);
         for(i=1; i< 1001; i++)
         {
            x = x3;
            y = y3;
            x3 = min + (max-min)*(double)(i)/1000.0;
            splint(tpoints, tdata, sderiv, Nx, x3, &y3);
            trap += 0.5*(x3-x)*(exp(x3)*y3+exp(x)*y);
         }

         fprintf(outfile, "%d %f %d %f\n", mc/1000, logLx, Nx, trap);

         scnt++;
         avr += trap;
         var += trap*trap;
      }
   } //End RJMCMC

   i = 0;
   for(ii=1; ii<iendNSP; ii++)
   {
      if(activex[ii] == 1)  // only use active points
      {
         i++;
         tpoints[i] = spoints[ii];
         tdata[i] = sdatax[ii];
      }
   }
   spline(tpoints, tdata, Nx, 1.0e31, 1.0e31, sderiv);

   sprintf(filename,"%s_bootstrap_fit.dat",modelname);
   outfile = fopen(filename,"w");
   for(i=1; i< 1001; i++)
   {
      x3 = min + (max-min)*(double)(i)/1000.0;
      splint(tpoints, tdata, sderiv, Nx, x3, &y3);
      fprintf(outfile,"%f %f\n", x3, y3);
   }
   fclose(outfile);

   x = avr/(double)(scnt);
   y = var/(double)(scnt);

   *logEvidence = x;
   *varLogEvidence = y-x*x;


   gsl_matrix_free(matrix);
   gsl_matrix_free(inverse);

   gsl_vector_free(eval);
   gsl_matrix_free(evec);

   gsl_permutation_free(perm);

   gsl_eigen_symmv_free(w);

  gsl_rng_free(seed);
  
   free_double_matrix(chains,ND-1);
   free_double_matrix(cov,ND-1);
   free_double_vector(mdl);

   free_double_vector(datax);
   free_double_vector(datay);
   free_double_vector(sigma);
   free_double_vector(counts);


   /* Free memory for spline fit*/
   free_double_vector(ref);
   free_double_vector(sprd);
   free_double_vector(spoints);
   free_double_vector(tdata);
   free_double_vector(tpoints);
   free_double_vector(sdatax);
   free_double_vector(sdatay);
   free_double_vector(sderiv);
   free_double_vector(sgm);
   free_double_vector(srm);

   free_int_vector(activex);
   free_int_vector(activey);
}

void ThermodynamicIntegration(double *temperature, double **loglikelihood, int M, int NC, char modelname[], double *logEvidence, double *varLogEvidence)
{
   int N, Nx, Ny, ND, NSP;
   int mc, i, j, ii, test, scnt;
   int acc;
   int fixedD;
   double logLx, logLy;
   double logpx, logpy;
   double *ref, *sprd, *datax, *datay;
   int *activex, *activey;
   int NL;
   double *counts;
   double alpha, H, av;
   double model;
   double max, min;
   double x, y, y3, y4, x3, x4, *sigma;
   double q, secd;
   double trap, dx;
   double *sdatay, *sderiv, *sgm, *srm;
   double *spoints, *sdatax, *tdata, *tpoints;
   double ep;
   double var, fird;
   double *mdl;
   double **cov, **icov, **chains;
   double E0, E1;
   double smooth;
   double a1, a2, base;
   double avr = 0.0;

   N = 10000000;  // number of MCMC steps

   //print logZchain to verify error estimates
   char filename[100];
   sprintf(filename,"chains/%s_evidence.dat",modelname);
   FILE *zFile = fopen(filename,"w");

   // sets the degree of smoothing for the spline
   smooth = 4.0;

   ep = 0.0001;

   ND = NC;
   NL = M;

  const gsl_rng_type *T = gsl_rng_default;
  gsl_rng *seed = gsl_rng_alloc(T);
  gsl_rng_env_setup();
  gsl_rng_set (seed, time(0));


   // maximum number of spline points - we initially overcover
   NSP = 2*ND-1;

   datax  = double_vector(ND-1);
   chains = double_matrix(ND-1,NL-1);
   datay  = double_vector(ND-1);
   icov   = double_matrix(ND-1,ND-1);
   cov    = double_matrix(ND-1,ND-1);
   counts = double_vector(NSP);
   sigma  = double_vector(ND-1);
   mdl    = double_vector(ND-1);

   for(j=0; j< ND; j++) datax[ND-1-j] = -log(temperature[j]);

   a1 = 0.0;
   a2 = 0.0;

   for(j=0; j< NL; j++)
   {
      for(i=0; i< ND; i++)
      {
         chains[ND-1-i][j] = loglikelihood[i][j];
         a1 += loglikelihood[i][j];
         a2 += 1.0;
      }
   }

   // subtract the mean, helps reduce trapazoid error
   a1 /= a2;
   base = a1*(1.0-exp(datax[0]));

   for(j=0; j< NL; j++)
   {
      for(i=0; i< ND; i++)
      {
         chains[ND-1-i][j] -= a1;
      }
   }

   bootcorrposdef(chains, ND, NL, cov, datay, seed);


   /******************************************/
   /*                                        */
   /*  Linear algebra gymnastics with the    */
   /*  correlation matrix                    */
   /*                                        */
   /******************************************/

   /* allocate memory for GSL linear algebra */

   gsl_matrix *matrix  = gsl_matrix_alloc (ND, ND);
   gsl_matrix *inverse = gsl_matrix_alloc (ND, ND);

   gsl_vector *eval = gsl_vector_alloc (ND);
   gsl_matrix *evec = gsl_matrix_alloc (ND, ND);

   gsl_permutation *perm = gsl_permutation_alloc(ND);

   gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc (ND);


   // get eigenvalues and eigenvectors of correlation matrix (matrix is destroyed)
   for (i = 0; i < ND; i++)
   {
      sigma[i] = sqrt(cov[i][i]);
      for (j = 0; j < ND; j++)
      {
         gsl_matrix_set(matrix,i,j,cov[i][j]);
      }
   }

  int matrix_condition_flag=1;

  /* 
   If the correlation matrix is singular we condition it by boosting
   the diagonal elements until the matrix is invertabl
   */
  while(matrix_condition_flag)
  {
    matrix_condition_flag=0;

    // the eigenstuff calculator
    gsl_eigen_symmv(matrix, eval, evec, w);

    /*
     if the correlation matrix is up to snuff
     calculate its inverse, otherwise condition
     */
    for(i=0; i<ND; i++)
    {
      if(gsl_vector_get(eval,i) < 0.0)
      {
        fprintf(stderr,"BayesWaveEvidence.c:1877: WARNING: singluar matrix, conditioning...\n");
        matrix_condition_flag = 1;
      }
      else
      {
        for(j=0; j<ND; j++) gsl_matrix_set(matrix,i,j,cov[i][j]);
      }
    }

    if(matrix_condition_flag)
    {
      for(i=0; i<ND; i++)
      {
        cov[i][i] *= 1.05;
        gsl_matrix_set(matrix,i,i,cov[i][i]);
      }
    }
    
  }

   // Make LU decomposition of matrix
   gsl_linalg_LU_decomp (matrix, perm, &i);

   // Invert the matrix
   gsl_linalg_LU_invert (matrix, perm, inverse);

   //get it in a form easily usable by the rest of the code
   for(i=0; i<ND; i++) for(j=0; j<ND; j++) icov[i][j] = gsl_matrix_get(inverse,i,j);
   // compute simple trapezoid integrand in beta
   trap = 0.0;
   for(j=1; j< ND; j++)
   {
      trap += 0.5*(exp(datax[j])-exp(datax[j-1]))*(datay[j]+datay[j-1]);
   }

   printf("Trapezoid integrand in beta %f\n", trap+base);

   max = datax[ND-1];
   min = datax[0];

   ref     = double_vector(NSP);
   sprd    = double_vector(NSP);
   spoints = double_vector(NSP);
   tdata   = double_vector(NSP);
   tpoints = double_vector(NSP);
   sdatax  = double_vector(NSP);
   sdatay  = double_vector(NSP);
   sderiv  = double_vector(NSP);
   sgm     = double_vector(NSP);
   srm     = double_vector(NSP);

   activex = int_vector(NSP);
   activey = int_vector(NSP);
  
   for(i=1; i<= NSP; i++)
   {
      activex[i] = 1;
      activey[i] = 1;
   }

   /* Initialize spline model */
   for(i=1; i<= ND; i++)
   {
      sdatax[2*i-1]= datay[i-1];
      spoints[2*i-1] = datax[i-1];
      sderiv[2*i-1] = 0.0;
      ref[2*i-1] = datay[i-1];
      sprd[2*i-1] = 2.0*sigma[i-1];
      if(2*i <= NSP)
      {
         sdatax[2*i]= 0.5*(datay[i-1]+datay[i]);
         spoints[2*i] = 0.5*(datax[i-1]+datax[i]);
         sderiv[2*i] = 0.0;
         ref[2*i] = sdatax[2*i];
         sprd[2*i] = 4.0*(sigma[i-1]+sigma[i]);
      }
   }

   spline(spoints, sdatax, NSP, 1.0e31, 1.0e31, sderiv);

   Nx = Ny = NSP;

   // set this flag to 1 if you want to do a fixed dimension run
   fixedD = 0;

   for(i=1; i<= NSP; i++) counts[i] = 0.0;


   /**************************************/
   /*                                    */
   /*  Initialize likelihood for MCMC    */
   /*                                    */
   /**************************************/

   for(j=0; j< ND; j++)
   {
      splint(spoints, sdatax, sderiv, Nx, datax[j], &model);
      mdl[j] = model;
   }

   trap = 0.0;
   x3 = min;
   splint(spoints, sdatax, sderiv, Nx, x3, &y3);
   for(i=1; i<= 1000; i++)
   {
      x = x3;
      y = y3;
      x3 = min + (max-min)*(double)(i)/1000.0;
      splint(spoints, sdatax, sderiv, Nx, x3, &y3);
      trap += 0.5*(x3-x)*(exp(x3)*y3+exp(x)*y);
   }

   for(i=1; i<= 1000; i++)
   {
      x3 = min + (max-min)*(double)(i)/1000.0;
      splint(spoints, sdatax, sderiv, Nx, x3, &y3);
   }

   av = 0.0;
   for (i = 0; i < ND; i++)
   {
      for (j = 0; j < ND; j++)
      {
         av += icov[i][j]*(datay[i]-mdl[i])*(datay[j]-mdl[j]);
      }
   }
   logLx = -av/2.0;

   logpx = 0.0;
   for(i=2; i< NSP; i++)
   {
      x = spoints[i];
      dx = spoints[i]-spoints[i-1];
      splint(spoints, sdatax, sderiv, Nx, x, &y);
      x3 = x - ep;
      splint(spoints, sdatax, sderiv, Nx, x3, &y3);
      x4 = x + ep;
      splint(spoints, sdatax, sderiv, Nx, x4, &y4);
      secd = (y3+y4-2.0*y)/(ep*ep);
      fird = (y4-y3)/(2.0*ep);
      logpx -= (smooth/(double)(NSP))*(secd*secd*dx*dx)/(fird*fird);

      // mononicity desired
      if(fird < 0.0) logpx -= 10.0;
   }

   acc   = 1;
   av    = 0.0;
   var   = 0.0;
   scnt  = 0;

   /****************/
   /*              */
   /*  The MCMC    */
   /*              */
   /****************/
   for(mc=0; mc< N; mc++)
   {

      test = 0;

      for(i=1; i<= NSP; i++)
      {
         sdatay[i] = sdatax[i];
         activey[i] = activex[i];
      }

      alpha = uniform_draw(seed);

      q = 0.5;
      if(fixedD == 1) q = 10.0;

      if(alpha > q)   // propose a dimension change
      {

         alpha = uniform_draw(seed);

         if(alpha < 0.5) Ny = Nx + 1;
         else            Ny = Nx - 1;

         if(Ny < Nx) // propose a kill
         {
            if(Ny > 1 && Ny <= NSP)
            {
               do i = 1 + (int)(uniform_draw(seed)*(double)(NSP)); // pick one to kill
               while(activex[i] == 0);  // can't kill it if already dead

               activey[i] = 0;
            }
            else test = 1;
         }
         else
         {
            if(Ny >= 1 && Ny < NSP)
            {

               do i = 1 + (int)(uniform_draw(seed)*(double)(NSP)); // pick one to add
               while(activex[i] == 1);  // can't add if already in use

               activey[i] = 1;
               sdatay[i] = ref[i]+10.0*sprd[i]*(1.0-2.0*uniform_draw(seed));
            }
            else test = 1;
         }
      }
      else     // within dimension update
      {
         Ny = Nx;

         alpha = uniform_draw(seed);

         for(ii=1; ii<= NSP; ii++)
         {
            // variety of jump sizes
            if(alpha > 0.8)          sdatay[ii] += sprd[ii]*gaussian_draw(seed);
            else if (alpha > 0.5)    sdatay[ii] += sprd[ii]*1.0e-1*gaussian_draw(seed);
            else if (alpha > 0.2)    sdatay[ii] += sprd[ii]*1.0e-2*gaussian_draw(seed);
            else                     sdatay[ii] += sprd[ii]*1.0e-3*gaussian_draw(seed);
         }
      }

      // check that proposed values are within the prior range
      if(Ny >= 1 && Ny <= NSP)
      {
         for(ii=1; ii<= NSP; ii++) // check the active points are in range
         {
            if(activey[ii] == 1)
            {
               if(sdatay[ii] > ref[ii]+10.0*sprd[ii]) test = 1;
               if(sdatay[ii] < ref[ii]-10.0*sprd[ii]) test = 1;
            }
         }
      }
      else test = 1;


      logLy = 0.0;

      if(test == 0)
      {

         i = 0;
         for(ii=1; ii<= NSP; ii++)
         {
            if(activey[ii] == 1)  // only use active points
            {
               i++;
               tpoints[i] = spoints[ii];
               tdata[i] = sdatay[ii];
            }
         }


         spline(tpoints, tdata, Ny, 1.0e31, 1.0e31, sderiv);
         for(j=0; j< ND; j++)
         {
            splint(tpoints, tdata, sderiv, Ny, datax[j], &model);
            mdl[j] = model;
         }
         av = 0.0;
         for (i = 0; i < ND; i++)
         {
            for (j = 0; j < ND; j++)
            {
               av += icov[i][j]*(datay[i]-mdl[i])*(datay[j]-mdl[j]);

            }
         }
         logLy = -av/2.0;

         logpy = 0.0;
         for(i=2; i< NSP; i++)
         {
            x = spoints[i];
            dx = spoints[i]-spoints[i-1];
            splint(tpoints, tdata, sderiv, Ny, x, &y);
            x3 = x - ep;
            splint(tpoints, tdata, sderiv, Ny, x3, &y3);
            x4 = x + ep;
            splint(tpoints, tdata, sderiv, Ny, x4, &y4);
            secd = (y3+y4-2.0*y)/(ep*ep);
            fird = (y4-y3)/(2.0*ep);
            logpy -= (smooth/(double)(NSP))*(secd*secd*dx*dx)/(fird*fird);
            
            // mononicity required
            if(fird < 0.0) logpy -= 10.0;
         }
      }
      
      H = logLy-logLx +logpy - logpx;
      
      alpha = log(uniform_draw(seed));
      
      if((H > alpha) && (test==0))
      {
         acc++;
         logLx = logLy;
         logpx = logpy;
         Nx = Ny;
         
         for(i=1; i<= NSP; i++)
         {
            sdatax[i] = sdatay[i];
            activex[i] = activey[i];
         }
      }
      
      // This gives us the data for Bayes factors. Don't start collecting it until after burn-in
      if(mc > N/2) counts[Nx] += 1.0;
      
      if(mc%1000 == 0)
      {
         i = 0;
         for(ii=1; ii<= NSP; ii++)
         {
            if(activex[ii] == 1)  // only use active points
            {
               i++;
               tpoints[i] = spoints[ii];
               tdata[i] = sdatax[ii];
            }
         }
         
         spline(tpoints, tdata, Nx, 1.0e31, 1.0e31, sderiv);
         
         trap = 0.0;
         x3 = min;
         splint(tpoints, tdata, sderiv, Nx, x3, &y3);
         for(i=1; i<= 1000; i++)
         {
            x = x3;
            y = y3;
            x3 = min + (max-min)*(double)(i)/1000.0;
            splint(tpoints, tdata, sderiv, Nx, x3, &y3);
            //trap += 0.5*(x3-x)*(exp(x3)*y3+exp(x)*y);
            trap += 0.5*(exp(x3)-exp(x))*(y3+y);
         }
         
         scnt++;
         avr += trap;
         var += trap*trap;
         
         splint(tpoints, tdata, sderiv, Nx, min, &E0);
         splint(tpoints, tdata, sderiv, Nx, max, &E1);
         
         fprintf(zFile,"%.12g\n",trap+base);
         
      }
   }
   fclose(zFile);
   
   i = 0;
   for(ii=1; ii<= NSP; ii++)
   {
      if(activex[ii] == 1)  // only use active points
      {
         i++;
         tpoints[i] = spoints[ii];
         tdata[i] = sdatax[ii];
      }
   }
   spline(tpoints, tdata, Nx, 1.0e31, 1.0e31, sderiv);
   
   for(i=1; i<= 1000; i++)
   {
      x3 = min + (max-min)*(double)(i)/1000.0;
      splint(tpoints, tdata, sderiv, Nx, x3, &y3);
   }
   
   x = avr/(double)(scnt);
   y = var/(double)(scnt);
   
   *logEvidence = x+base;
   *varLogEvidence = y-x*x;
   
   
   gsl_matrix_free(matrix);
   gsl_matrix_free(inverse);
   gsl_vector_free(eval);
   gsl_matrix_free(evec);
   gsl_permutation_free(perm);
   gsl_eigen_symmv_free(w);
   
   
   free_double_matrix(chains,ND-1);
   free_double_matrix(icov,ND-1);
   free_double_matrix(cov,ND-1);
   
   free_double_vector(datax);
   free_double_vector(datay);
   free_double_vector(counts);
   free_double_vector(sigma);
   free_double_vector(mdl);
   
   free_double_vector(ref);
   free_double_vector(sprd);
   free_double_vector(spoints);
   free_double_vector(tdata);
   free_double_vector(tpoints);
   free_double_vector(sdatax);
   free_double_vector(sdatay);
   free_double_vector(sderiv);
   free_double_vector(sgm);
   free_double_vector(srm);
   
   free_int_vector(activex);
   free_int_vector(activey);
   
}

void TrapezoidIntegration(struct Chain *chain, int count, char modelname[], double *logEvidence, double *varLogEvidence)
{
   int i,ic;
   int NC=chain->NC;

   double varZ=0.0;
   double logZ=0.0;
   /*
    Directly downsample chain to Neff = 100
    Compute <logL> from downsampled chain and var(<logL>) = var(logL)/Neff
    If ACL ~ Nsamples then use var(<logL>) = var(logL)
    */
   double *ACL = malloc(NC*sizeof(double));
   for(i=0; i<NC; i++) ACL[i] = 0.0;
   average_log_likelihood_via_direct_downsampling(chain, ACL, count);
   
   //print logZ integrand
   char filename[100];
   sprintf(filename,"%s_evidence.dat",modelname);
   FILE *zFile = fopen(filename,"w");
   for(ic=0; ic<NC; ic++) fprintf(zFile,"%.12g %.12g %.12g %g\n", 1./chain->temperature[ic], chain->avgLogLikelihood[ic], sqrt(chain->varLogLikelihood[ic]), ACL[ic]);
   fclose(zFile);
   free(ACL);
   
   /******************************************************************************/
   /*                                                                            */
   /*  Trapezoid integration of <logL> dBeta                                     */
   /*  Obsolete thanks to Neil's Monte Carlo integration of the output files     */
   /*  which will eventually get ported into BayesWave as a function             */
   /*                                                                            */
   /******************************************************************************/
   double beta[NC];
   for(ic=0; ic<NC; ic++)
      beta[ic] = 1./chain->temperature[ic];
   
   double dBeta= 0.0;
   
   for(ic=0; ic<NC-1; ic++)
   {
      dBeta = beta[ic]-beta[ic+1];
      logZ += (chain->avgLogLikelihood[ic]+chain->avgLogLikelihood[ic+1])*dBeta/2.0;
      if(ic==0)
         varZ += chain->varLogLikelihood[ic]*dBeta*dBeta/4.0;
      else
         varZ += chain->varLogLikelihood[ic]*pow(beta[ic+1]-beta[ic-1],2)/4.0;
   }
   varZ += chain->varLogLikelihood[NC-1]*pow(beta[NC-2]-beta[NC-1],2)/4.0;
   
   *logEvidence    = logZ;
   *varLogEvidence = varZ;
   
}

void LaplaceApproximation(struct Data *data, struct Model *model, struct Chain *chain, struct Prior *prior, double *logEvidence)
{
   int i,j,ii, ifo;
   int NI = data->NI;
    int NW = data->NW;
    
   double logP;
   double detC=0;
   double logV=0;
   double logVsignal;
   double logVglitch;
   double *params=malloc(NW*sizeof(double));
   double *sigmas=malloc(NW*sizeof(double));
   double minAmp;
   double maxAmp;

   int dim=0;

   /*********************************/
   /*                               */
   /*  MAP Posterior                */
   /*                               */
   /*********************************/
   logP = model->logL;
   if(data->glitchFlag) for(ifo=0; ifo<NI; ifo++) logP += model->glitch[ifo]->logp;
   if(data->signalFlag) logP += model->signal[0]->logp;

   /*********************************/
   /*                               */
   /*  Posterior Volume             */
   /*                               */
   /*********************************/
   if(data->signalFlag)
   {
      extrinsic_fisher_update(data, model);
      for(j=0; j<4; j++) detC -= log(model->fisher->evalue[j])/2.;

      //printf("     MAP signal model dimension:  %i wavelets\n",model->signal[0]->size);
      for(i=1; i<=model->signal[0]->size; i++)
      {
         ii = model->signal[0]->index[i];
         intrinsic_fisher_update(model->signal[0]->intParams[ii], sigmas, model->SnGeo, data->Tobs, data->NW,1 );
         for(j=0; j<NW; j++) detC += log(sigmas[j]);
      }
   }
   if(data->glitchFlag)
   {
      for(ifo=0; ifo<NI; ifo++)
      {
         //printf("     MAP glitch model dimension IFO%i:  %i wavelets\n",ifo,model->glitch[ifo]->size);
         for(i=1; i<=model->glitch[ifo]->size; i++)
         {
            ii = model->glitch[ifo]->index[i];
            //for(j=0; j<NW; j++)printf("     %i:  %g\n",j,model->glitch[ifo]->intParams[ii][j]);
            intrinsic_fisher_update(model->glitch[ifo]->intParams[ii], sigmas, model->Snf[ifo], data->Tobs, data->NW, 1);
            //for(j=0; j<NW; j++)printf("     %i:  %g\n",j,sigmas[j]);
            for(j=0; j<NW; j++) detC += log(sigmas[j]);
         }
      }
   }
   /*********************************/
   /*                               */
   /*  Prior Volume                 */
   /*                               */
   /*********************************/
   /*
    if(data->psdFitFlag)
    {
    dim += NI; //PSDscale
    for(ifo=0; ifo<NI; ifo++) logV += log(prior->Snmax-prior->Snmin);
    }
    */
   if(data->signalFlag)
   {
      //extrinsic
      dim+=4;
      logV += log(LAL_TWOPI); //r.a.
      logV += log(2.);        //sindec
      logV += log(LAL_TWOPI); //psi
      logV += log(2.);        //ecc

      //intrinsic
      dim+=NW*model->signal[0]->size;

      logVsignal = 0;

      logVsignal+=log(prior->range[0][1]-prior->range[0][0]); //time
      logVsignal+=log(prior->range[1][1]-prior->range[1][0]); //frequency
      logVsignal+=log(prior->range[2][1]-prior->range[2][0]); //Q

      // Monte Carlo estimation of amplitude prior volume
      minAmp= 1.e60;
      maxAmp=-1.e60;
      for(i=0; i<100; i++)
      {
         draw_wavelet_params(params, prior->range, chain->seed, data->NW);
         data->signal_amplitude_proposal(params, model->SnGeo, chain->seed, data->Tobs, prior->range,prior->sSNRpeak);
         if(params[3]<minAmp)minAmp=params[3];
         if(params[3]>maxAmp)maxAmp=params[3];
      }

      logVsignal+=log(maxAmp-minAmp);                         //amplitude
      logVsignal+=log(prior->range[4][1]-prior->range[4][0]); //phase

      logV += model->signal[0]->size*logVsignal;
   }
   if(data->glitchFlag)
   {
      for(ifo=0; ifo<NI; ifo++)
      {
         dim+=NW*model->glitch[ifo]->size; //intrinsic

         logVglitch = 0;

         logVglitch+=log(prior->range[0][1]-prior->range[0][0]); //time
         logVglitch+=log(prior->range[1][1]-prior->range[1][0]); //frequency
         logVglitch+=log(prior->range[2][1]-prior->range[2][0]); //Q

         minAmp= 1.e60;
         maxAmp=-1.e60;
         for(i=0; i<100; i++)
         {
            draw_wavelet_params(params, prior->range, chain->seed, data->NW);
            data->glitch_amplitude_proposal(params, model->Snf[ifo], chain->seed, data->Tobs, prior->range, prior->gSNRpeak);
            if(params[3]<minAmp)minAmp=params[3];
            if(params[3]>maxAmp)maxAmp=params[3];
         }

         logVglitch+=log(maxAmp-minAmp);                         //amplitude
         logVglitch+=log(prior->range[4][1]-prior->range[4][0]); //phase

         logV += model->glitch[ifo]->size*logVglitch;

      }
   }
   
   /*********************************/
   /*                               */
   /*  Laplace-Fisher Evidence      */
   /*                               */
   /*********************************/
   
   *logEvidence = logP + detC - logV + 0.5*(double)dim*log(LAL_TWOPI);
   free(params);
   free(sigmas);
}
