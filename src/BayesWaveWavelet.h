/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg, Margaret Millhouse
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */


/* ********************************************************************************** */
/*                                                                                    */
/*                            Wavelet basis functions                                 */
/*                                                                                    */
/* ********************************************************************************** */

double SineGaussianSNR(double *params, double *Snf, double Tobs);
void SineGaussianBandwidth(double *sigpar, double *fmin, double *fmax);
void SineGaussianFourier(double *hs, double *sigpar, int N, int flag, double Tobs);
void SineGaussianFisher(double *params, struct FisherMatrix *fisher);

double ChirpletSNR(double *params, double *Snf, double Tobs);
void ChirpletBandwidth(double *sigpar, double *fmin, double *fmax);
void ChirpletFourier(double *hs, double *sigpar, int N, int flag, double Tobs);
void ChirpletFisher(double *params, struct FisherMatrix *fisher);

/* ********************************************************************************** */
/*                                                                                    */
/*                         Network projection functions                               */
/*                                                                                    */
/* ********************************************************************************** */

void computeProjectionCoeffs(struct Data *data, struct Network *projection, double *extParams, double fmin, double fmax);
void waveformProject(struct Data *data, struct Network *projection, double *extParams, double **response, double **h, double fmin, double fmax);
void combinePolarizations(struct Data *data, struct Wavelet **geo, double **h, double *extParams, int Npol);
void compute_reconstructed_plus_and_cross(struct Data *data, struct Network *projection, double *extParams, double **hrecPlus, double **hrecCross, double *geo, double fmin, double fmax);
