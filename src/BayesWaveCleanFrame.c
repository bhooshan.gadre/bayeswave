/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg, James A. Clark, Jonah B. Kanner
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

/***************************  REQUIRED LIBRARIES  ***************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <fftw3.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sort_double.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_cdf.h>

#include <lal/LALCache.h>
#include <lal/LALFrStream.h>
#include <lal/TimeSeries.h>
#include <lal/XLALError.h>
#include <lal/Date.h>


/*************  PROTOTYPE DECLARATIONS FOR INTERNAL FUNCTIONS  **************/

#define REQARG 12
#define MAXSTRINGSIZE 1024

struct Data
{
  int medianFlag;

  //Set data products from command line
  char ifo[4];        //interferometer (e.g V1);

  char fr_cache[MAXSTRINGSIZE];//cache file (e.g. V1.cache)
  char fr_chanl[MAXSTRINGSIZE];//channel type (e.g. V1:Hrec_hoft_16384Hz)
  char fr_type[MAXSTRINGSIZE]; //frame type (e.g. V1_llhoft)

  double fr_seglen;   //frame segment length
  double fr_start;    //frame start time
  double fr_srate;    //frame sampling rate

  char bw_model[MAXSTRINGSIZE]; //bayeswave glitch model

  double bw_seglen;   //bayeswave segment length
  double bw_start;    //bayeswave start time
  double bw_srate;    //bayeswave sampling rate
  double bw_trigtime; //bayeswave trigger time

  //Derived data products
  int bw_N;           //bayeswave number of samples
  double bw_dt;       //bayeswave sample length

};

void print_usage();
void parse_command_line(int argc, char **argv, struct Data *data);

static REAL8TimeSeries *readTseries(CHAR *cachefile, CHAR *channel, LIGOTimeGPS start, REAL8 length);

static void output_frame(REAL8TimeSeries *timeData, REAL8TimeSeries *timeRes, REAL8TimeSeries *timeGlitch, CHAR *frameType, CHAR *ifo);

void fftw_wrapper(double *data, int N, int flag);

void tukey_scale(double *s1, double *s2, double alpha, int N);
void tukey(double *data, double alpha, int N);


/* ============================  MAIN PROGRAM  ============================ */


int main(int argc, char *argv[])
{
  fprintf(stdout,"\n");
  fprintf(stdout,"#############################################################\n");
  fprintf(stdout,"                BayesWave Glitch Subtraction                 \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                             **                              \n");
  fprintf(stdout,"                            * *                              \n");
  fprintf(stdout,"****************************  *   ***************************\n");
  fprintf(stdout,"                              * **                           \n");
  fprintf(stdout,"                              **                             \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                              *                              \n");
  fprintf(stdout,"                                                             \n");
  fprintf(stdout,"#############################################################\n");
  fprintf(stdout,"\n");

  /*   Variable declaration   */
  int i, j, k, N;

  double x;
  double dT;

  double alpha;

  int Nup;
  double norm;
  double s1, s2;
  double t_rise;

  struct Data *data = malloc(sizeof(struct Data));

  FILE *infile;

  // ------------------------------------------------- //
  parse_command_line(argc, argv, data);

  REAL8TimeSeries* timeRes=NULL;
  REAL8TimeSeries* timeData=NULL;
  REAL8TimeSeries* timeGlitch=NULL;
  LIGOTimeGPS epoch;

  // Set time & retrieve data by reading frame cache
  XLALGPSSetREAL8(&epoch, data->fr_start);
  fprintf(stdout,"Reading original data:\n");
  fprintf(stdout,"     Type:    %s\n",data->fr_type);
  fprintf(stdout,"     Channel: %s\n",data->fr_chanl);
  fprintf(stdout,"\n");
  timeRes    = readTseries(data->fr_cache,data->fr_chanl,epoch,data->fr_seglen);
  timeData   = readTseries(data->fr_cache,data->fr_chanl,epoch,data->fr_seglen);
  timeGlitch = readTseries(data->fr_cache,data->fr_chanl,epoch,data->fr_seglen);

  char * version="T1700406_v3";

  char outframeType[MAXSTRINGSIZE];
  char outframeChannel[MAXSTRINGSIZE];
  char outframeGlitchChannel[MAXSTRINGSIZE];

  snprintf(outframeType, MAXSTRINGSIZE, "%s_%s", data->fr_type,  version);
  snprintf(outframeChannel, MAXSTRINGSIZE, "%s_%s", data->fr_chanl, version);
  snprintf(outframeGlitchChannel, MAXSTRINGSIZE, "%s_glitch", data->fr_chanl);

  /* set the channel name */
  strncpy(timeData->name,   data->fr_chanl,        LALNameLength);
  strncpy(timeRes->name,    outframeChannel,       LALNameLength);
  strncpy(timeGlitch->name, outframeGlitchChannel, LALNameLength);

  
  fprintf(stdout,"Creating glitch-subtracted frame:\n");
  fprintf(stdout,"     Type:    %s\n",outframeType);
  fprintf(stdout,"     Channel: %s\n",outframeChannel);
  fprintf(stdout,"\n");

  // shortcuts to original frame size/cadence
  dT = timeData->deltaT;
  N  = timeData->data->length;

  // parameters for BW glitch data
  data->bw_dt = 1.0/data->bw_srate;
  data->bw_N  = data->bw_seglen/data->bw_dt;

  // Length of the up-sampled glitch data
  Nup = (int)(data->bw_seglen*data->fr_srate);
  if(Nup<data->bw_N)
  {
    fprintf(stdout,"\nbayeswave_cleanframe: model has higher sampling rate than frame\n\n");
    exit(0);
  }

  double *clean_data             = malloc(sizeof(double)*N);
  double *glitch_time            = malloc(sizeof(double)*data->bw_N);
  double *glitch_model           = malloc(sizeof(double)*data->bw_N);
  double *glitch_model_resampled = malloc(sizeof(double)*Nup);
  double *glitch_model_padded    = malloc(sizeof(double)*N);
  double **glitch_array          = malloc(sizeof(double *)*data->bw_N);
  for(i=0; i<data->bw_N; i++) glitch_array[i] = malloc(sizeof(double)*100);

  // read in BW glitch reconstruction
  if(data->medianFlag)
  {
    //split glitch model file name
    char start[MAXSTRINGSIZE];
    char extension[MAXSTRINGSIZE];
    j = 0;
    while(data->bw_model[j]!='1')
    {
      start[j]=data->bw_model[j];
      j++;
    }
    //that first 1 was from the IFO name.  Do it again
    start[j]=data->bw_model[j];
    j++;
    while(data->bw_model[j]!='1')
    {
      start[j]=data->bw_model[j];
      j++;
    }
    //terminate the string
    start[j]=0;

    //burn off next two digits
    j+=3;
    i=0;
    while(j<MAXSTRINGSIZE)
    {
      extension[i]=data->bw_model[j];
      j++;
      i++;
    }

    fprintf(stdout,"Using median glitch reconstruction:\n");
    fprintf(stdout,"     Files: %s[%i-%i]%s\n",start,100,199,extension);
    fprintf(stdout,"\n");

    //read all waveform reconstruction into array
    char filename[MAXSTRINGSIZE];
    for(j=0; j<100; j++)
    {
      sprintf(filename,"%s%i%s",start,100+j,extension);
      infile=fopen(filename,"r");
      for(i=0; i< data->bw_N; i++)
      {
        fscanf(infile,"%lf%lf", &x, &glitch_array[i][j]);
        if(j==0)glitch_time[i] = data->bw_start + (double)(i)*data->bw_dt;
      }
      fclose(infile);
    }

    //get median of each time bin
    for(i=0; i<data->bw_N; i++)
    {
      gsl_sort(glitch_array[i], 1, 100);
      glitch_model[i] = gsl_stats_median_from_sorted_data(glitch_array[i], 1, 100);
    }
  }
  else
  {
    infile = fopen(data->bw_model,"r");

    for(i=0; i< data->bw_N; i++)
    {
      fscanf(infile,"%lf%lf", &x, &glitch_model[i]);
      glitch_time[i] = data->bw_start + (double)(i)*data->bw_dt;
    }
    fclose(infile);
  }
  
  /***********************************************************
   zero-pad FD glitch model to match sampling rate of data
   ************************************************************/
  printf("zero-padding FD glitch model\n");
  
  // Set up Tukey window parameter. Flat for (1-alpha) seconds of data
  t_rise = 0.4;  // the standard LIGO choice
  alpha  = (2.0*t_rise/data->bw_seglen);

  // Tukey filter the glitch reconstruction to avoid edge effects
  tukey(glitch_model, alpha, data->bw_N);

  // FFT
  fftw_wrapper(glitch_model, data->bw_N, 1);

  // FFT scaling
  norm = 1.0/(double)data->bw_N;

  // zero pad the high frequencies
  for(i=0; i< data->bw_N; i++)
  {
    glitch_model[i] *= norm;
    glitch_model_resampled[i] = glitch_model[i];
  }
  for(i=data->bw_N; i< Nup; i++)
  {
    glitch_model_resampled[i] = 0.0;
  }

  // Inverse FFT
  fftw_wrapper(glitch_model_resampled, Nup, -1);


  /***********************************************************
   glitch_model_resampled now contains the glitch model,
   upsampled to the original cadence
   ************************************************************/
  printf("aligning glitch model in full data array\n");

  // figure out where to place the model in the frame
  j = (int)((data->bw_start-data->fr_start)/dT);
  k = j+Nup;

  //zero pad array
  for(i=0; i< N; i++) glitch_model_padded[i] = 0.0;

  //place glitch model in right part of full array
  for(i=j; i< k; i++)
  {
    glitch_model_padded[i] = glitch_model_resampled[i-j];
  }

  /***********************************************************
   glitch_model_padded now contains the glitch model,
   over the same interval & cadence as the original frame
  ************************************************************/
  printf("creating residual\n");

  //renormalize glitch model amplitude (undo windowing)
  tukey_scale(&s1, &s2, alpha, N);

  norm = sqrt(0.5/(data->bw_dt*data->bw_seglen))*s2;

  // Initialise and populate clean data
  for(i=0; i< N; i++)
  {
    //store clean data in it's own array for spectrogram making
    clean_data[i] = timeData->data->data[i] - norm*glitch_model_padded[i];

    //fill LAL data structure with cleaned data for frame-making
    timeRes->data->data[i] = clean_data[i];
    
    //fill another structure with glitch model
    timeGlitch->data->data[i] = norm*glitch_model_padded[i];
  }


  // Output cleaned data!
  output_frame(timeData, timeRes, timeGlitch, outframeType, data->ifo);
  XLALDestroyREAL8TimeSeries(timeData);

  fprintf(stdout,"\n");
  return 0;
}

void tukey_scale(double *s1, double *s2, double alpha, int N)
{
  int i, imin, imax;
  double x1, x2;
  double filter;

  imin = (int)(alpha*(double)(N-1)/2.0);
  imax = (int)((double)(N-1)*(1.0-alpha/2.0));

  x1 = 0.0;
  x2 = 0.0;
  for(i=0; i< N; i++)
  {
    filter = 1.0;
    if(i < imin) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-1.0 )));
    if(i>imax) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-2.0/alpha+1.0 )));
    x1 += filter;
    x2 += filter*filter;
  }
  x1 /= (double)(N);
  x2 /= (double)(N);

  *s1 = x1;
  *s2 = sqrt(x2);

}

void tukey(double *data, double alpha, int N)
{
  int i, imin, imax;
  double filter;

  imin = (int)(alpha*(double)(N-1)/2.0);
  imax = (int)((double)(N-1)*(1.0-alpha/2.0));

  for(i=0; i< N; i++)
  {
    filter = 1.0;
    if(i < imin) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-1.0 )));
    if(i>imax) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-2.0/alpha+1.0 )));
    data[i] *= filter;
  }

}


/* ********************************************************************************** */
/*																					  */
/*                                 Fourier Routines                                   */
/*																					  */
/* ********************************************************************************** */


void fftw_wrapper(double *data, int N, int flag)
{
  int n;
  double *timeData = (double *)malloc(N*sizeof(double));
  fftw_complex *freqData = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);

  //setup FFTW plans
  fftw_plan reverse = fftw_plan_dft_c2r_1d(N, freqData, timeData, FFTW_MEASURE);
  fftw_plan forward = fftw_plan_dft_r2c_1d(N, timeData, freqData, FFTW_MEASURE);

  switch (flag)
  {
    //Reverse transform
    case -1:
      
      //fill freqData with contents of data
      for(n=0; n<N/2; n++)
      {
        freqData[n][0] = data[2*n];
        freqData[n][1] = data[2*n+1];
      }
      
      //get DC and Nyquist where FFTW wants them
      freqData[N/2][0] = freqData[0][1];
      freqData[N/2][1] = freqData[0][1] = 0.0;


      //The FFT
      fftw_execute(reverse);

      //Copy output back into data
      for(n=0; n<N; n++) data[n] = timeData[n];

      break;
      
    //Forward transform
    case 1:

      //fill timeData with contents of data
      for(n=0; n<N; n++) timeData[n] = data[n];

      //fill timeData with contents of data
      for(n=0; n<N; n++) timeData[n] = data[n];

      //The FFT
      fftw_execute(forward);

      //Copy output back into data
      for(n=0; n<N/2; n++)
      {
        data[2*n]   = freqData[n][0];
        data[2*n+1] = freqData[n][1];
      }
      
      //get DC and Nyquist where FFTW wants them
      data[1] = freqData[N/2][1];

      break;
      
    default:
      fprintf(stdout,"Error: unsupported type in fftw_wrapper()\n");
      exit(1);
      break;
  }

  fftw_destroy_plan(reverse);
  fftw_destroy_plan(forward);

  free(timeData);
  fftw_free(freqData);
}

/* ********************************************************************************** */
/*																					  */
/*                                 Frame I/O                                          */
/*																					  */
/* ********************************************************************************** */

static REAL8TimeSeries *readTseries(CHAR *cachefile, CHAR *channel, LIGOTimeGPS start, REAL8 length)
{
  LALStatus status;
  memset(&status,0,sizeof(status));
  LALCache *cache = NULL;
  LALFrStream *stream = NULL;
  REAL8TimeSeries *out = NULL;

  cache  = XLALCacheImport( cachefile );
  int err;
  err = *XLALGetErrnoPtr();
  if(cache==NULL) {fprintf(stderr,"ERROR: Unable to import cache file \"%s\",\n       XLALError: \"%s\".\n",cachefile, XLALErrorString(err)); exit(-1);}
  stream = XLALFrStreamCacheOpen( cache );
  if(stream==NULL) {fprintf(stderr,"ERROR: Unable to open stream from frame cache file\n"); exit(-1);}
  out = XLALFrStreamInputREAL8TimeSeries( stream, channel, &start, length , 0 );
  if(out==NULL) fprintf(stderr,"ERROR: unable to read channel %s from %s at time %i\nCheck the specified data duration is not too long\n",channel,cachefile,start.gpsSeconds);
  XLALDestroyCache(cache);
  LALFrClose(&status,&stream);
  return out;
}


static void output_frame(REAL8TimeSeries *timeData,
                         REAL8TimeSeries *timeRes,
                         REAL8TimeSeries *timeGlitch,
                         CHAR *frameType,
                         CHAR *ifo)
{
  CHAR fname[2048];
  INT4 duration;
  INT8 detectorFlags;
  LALFrameH *frame;

  int gpsStart = timeData->epoch.gpsSeconds;
  int gpsEnd = gpsStart + (int)timeData->data->length*timeData->deltaT;


  /* set detector flags */
  if ( strncmp( ifo, "H2", 2 ) == 0 )
    detectorFlags = LAL_LHO_2K_DETECTOR_BIT;
  else if ( strncmp( ifo, "H1", 2 ) == 0 )
    detectorFlags = LAL_LHO_4K_DETECTOR_BIT;
  else if ( strncmp( ifo, "L1", 2 ) == 0 )
    detectorFlags = LAL_LLO_4K_DETECTOR_BIT;
  else if ( strncmp( ifo, "G1", 2 ) == 0 )
    detectorFlags = LAL_GEO_600_DETECTOR_BIT;
  else if ( strncmp( ifo, "V1", 2 ) == 0 )
    detectorFlags = LAL_VIRGO_DETECTOR_BIT;
  else if ( strncmp( ifo, "T1", 2 ) == 0 )
    detectorFlags = LAL_TAMA_300_DETECTOR_BIT;
  else
  {
    fprintf( stderr, "ERROR: Unrecognised IFO: '%s'\n", ifo );
    exit( 1 );
  }

  /* get frame filename */
  duration = gpsEnd - gpsStart;
  snprintf( fname, FILENAME_MAX, "%c-%s-%d-%d.gwf", ifo[0], frameType, gpsStart, duration );

  /* define frame */
  frame = XLALFrameNew( &timeData->epoch, duration, "LIGO", 0, 1, detectorFlags );

  /* add channel to frame */
  XLALFrameAddREAL8TimeSeriesSimData( frame, timeRes );
  XLALFrameAddREAL8TimeSeriesSimData( frame, timeGlitch );
  XLALFrameAddREAL8TimeSeriesSimData( frame, timeData );

  fprintf( stdout, "Writing data to frame: '%s'\n", fname );

  /* write frame */
  if (XLALFrameWrite( frame, fname) != 0)
  {
    fprintf( stderr, "ERROR: Cannot save frame file: '%s'\n", fname );
    exit( 1 );
  }

  /* clear frame */
  XLALFrameFree( frame );

  return;
}

void parse_command_line(int argc, char **argv, struct Data *data)
{
  int i;

  if(argc==1)
  {
    print_usage();
    exit(0);
  }

  FILE *outfile = fopen("bayeswave_cleanframe.run","w");
  for(i=0; i<argc; i++) fprintf(outfile,"%s ",argv[i]);
  fprintf(outfile,"\n");
  fclose(outfile);

  data->medianFlag = 0;

  static struct option long_options[] =
  {
    {"ifo",           required_argument, 0, 0},
    {"glitch-model",  required_argument, 0, 0},
    {"cachefile",     required_argument, 0, 0},
    {"channel",       required_argument, 0, 0},
    {"frame-type",    required_argument, 0, 0},
    {"frame-length",  required_argument, 0, 0},
    {"frame-start",   required_argument, 0, 0},
    {"frame-srate",   required_argument, 0, 0},
    {"seglen",        required_argument, 0, 0},
    {"srate",         required_argument, 0, 0},
    {"segment-start", required_argument, 0, 0},
    {"trigtime",      required_argument, 0, 0},
    {"median",        no_argument, 0, 0},
    {"help",          no_argument, 0,'h'},
    {0, 0, 0, 0}
  };

  int opt=0;
  int long_index=0;

  int argCount = 0;
  int argcheck[REQARG];
  for(i=0; i<REQARG; i++) argcheck[i] = 0;

  //Loop through argv string and pluck out arguments
  while ((opt = getopt_long_only(argc, argv,"apl:b:", long_options, &long_index )) != -1)
  {
    switch (opt)
    {

      case 0:
        if(strcmp("ifo",           long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->ifo, "%s", optarg);
        }
        if(strcmp("glitch-model",  long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->bw_model, "%s", optarg);
        }
        if(strcmp("cachefile", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->fr_cache, "%s", optarg);
        }
        if(strcmp("channel", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->fr_chanl, "%s", optarg);
        }
        if(strcmp("frame-type", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          sprintf(data->fr_type, "%s", optarg);
        }
        if(strcmp("frame-length", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->fr_seglen = (double)atof(optarg);
        }
        if(strcmp("frame-start", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->fr_start = (double)atof(optarg);
        }
        if(strcmp("frame-srate", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->fr_srate = (double)atof(optarg);
        }
        if(strcmp("seglen", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->bw_seglen = (double)atof(optarg);
        }
        if(strcmp("srate", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->bw_srate = (double)atof(optarg);
        }
        if(strcmp("segment-start", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->bw_start = (double)atof(optarg);
        }
        if(strcmp("trigtime", long_options[long_index].name) == 0)
        {
          argcheck[argCount]++;
          argCount++;
          data->bw_trigtime = (double)atof(optarg);
        }
        if(strcmp("median",long_options[long_index].name) == 0)
        {
          data->medianFlag = 1;
        }
        if(strcmp("help", long_options[long_index].name) == 0)
        {
          fprintf(stdout,"BayesWaveGlitchFrame:\n");
          fprintf(stdout,"  Create glitch-subtracted frames from BayesWave residual\n");
          fprintf(stdout,"\n");
          print_usage();
          exit(0);
        }
        break;
      case 'h':
        fprintf(stdout,"BayesWaveGlitchFrame:\n");
        fprintf(stdout,"  Create glitch-subtracted frames from BayesWave residual\n");
        fprintf(stdout,"\n");
        print_usage();
        exit(0);
        break;
      default: print_usage();
        exit(0);
    }
  }

  //make sure there all the right arguments were used
  for(i=0; i<REQARG; i++)
  {
    if(argcheck[i]!=1)
    {
      fprintf(stdout,"\nbayeswave_cleanframe: missing requird argument\n\n");
      print_usage();
      exit(0);
    }
  }

  //See how we did with th command line
  fprintf(stdout,"interferometer.............%s\n",data->ifo);
  fprintf(stdout,"\n");
  fprintf(stdout,"cache file.................%s\n",data->fr_cache);
  fprintf(stdout,"channel type...............%s\n",data->fr_chanl);
  fprintf(stdout,"frame type.................%s\n",data->fr_type);
  fprintf(stdout,"\n");
  fprintf(stdout,"frame segment length.......%li\n",(long)data->fr_seglen);
  fprintf(stdout,"frame start time...........%li\n",(long)data->fr_start);
  fprintf(stdout,"frame sampling rate........%li\n",(long)data->fr_srate);
  fprintf(stdout,"\n");
  fprintf(stdout,"bayeswave glitch model.....%s\n",data->bw_model);
  fprintf(stdout,"bayeswave segment length...%li\n",(long)data->bw_seglen);
  fprintf(stdout,"bayeswave start time.......%li\n",(long)data->bw_start);
  fprintf(stdout,"bayeswave sampling rate....%li\n",(long)data->bw_srate);
  fprintf(stdout,"bayeswave trigger time.....%li\n",(long)data->bw_trigtime);
  fprintf(stdout,"\n");
}

void print_usage()
{
  fprintf(stdout,"Requird Arguments:\n");
  fprintf(stdout,"--ifo           interferometers (H1,L1,..)\n");
  fprintf(stdout,"--glitch-model  glitch reconstruction\n");
  fprintf(stdout,"--cachefile     cache files pointing to original frames\n");
  fprintf(stdout,"--channel       data channel name in original frames\n");
  fprintf(stdout,"--frame-type    original frame type\n");
  fprintf(stdout,"--frame-length  frame length\n");
  fprintf(stdout,"--frame-start   frame start time\n");
  fprintf(stdout,"--frame-srate   frame sammpling rate\n");
  fprintf(stdout,"--seglen        bayeswave segment length\n");
  fprintf(stdout,"--srate         bayeswave sampling rate\n");
  fprintf(stdout,"--segment-start bayeswave segment start time\n");
  fprintf(stdout,"--trigtime      bayeswave trigger time\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"Optional Arguments:\n");
  fprintf(stdout,"--median        use median glitch reconstruction\n");
  fprintf(stdout,"                can only be used on a completed run\n");
  fprintf(stdout,"--help          print help output\n");
  fprintf(stdout,"\n");
}

