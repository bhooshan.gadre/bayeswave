FROM containers.ligo.org/lscsoft/lalsuite/lalsuite-v6.53:el7
ARG version
RUN echo "Building bayeswave"
MAINTAINER James Alexander Clark <james.clark@ligo.org>

RUN yum upgrade -y && \
      yum install -y gcc \
      cmake3 \
      gcc-c++ \
      help2man \
      rpm-build \
      which \
      git \
      python-ligo-lw && \
      yum clean all && \
      rm -rf /var/cache/yum

# RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
#     python get-pip.py && \
#     rm get-pip.py

# Copy and build BayesWave
WORKDIR /tmp
RUN mkdir /tmp/bayeswave
COPY bayeswave.spec.in /tmp/bayeswave
COPY .git /tmp/bayeswave
COPY CMakeLists.txt /tmp/bayeswave
COPY src /tmp/bayeswave/src
COPY etc/bayeswave-user-env.sh /tmp/bayeswave/etc/bayeswave-user-env.sh
COPY README.md /tmp
COPY BayesWaveUtils /tmp/bayeswave/BayesWaveUtils
ADD test/test-bayeswave.sh /

# Python Utils
RUN cd /tmp/bayeswave/BayesWaveUtils && \
      python /tmp/bayeswave/BayesWaveUtils/setup.py install 

RUN mkdir -p ~/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
RUN echo '%_topdir %(echo $HOME)/rpmbuild' > ~/.rpmmacros
RUN mkdir dist && \
      pushd dist && \
      cmake3 /tmp/bayeswave && \
      cmake3 --build . --target package_source && \
      mv bayeswave-*.tar.xz /root/rpmbuild/SOURCES/ && \
      popd
RUN rpmbuild -ba /tmp/bayeswave/bayeswave.spec
RUN rpm -ivh /root/rpmbuild/RPMS/x86_64/bayeswave-1.0.3-1.el7.x86_64.rpm

RUN rm -rf .git install.sh /src /BayesWaveUtils /etc/bayeswave-user-env.sh /build.sh /CMakeLists.txt /build
RUN mkdir -p /cvmfs /hdfs /hadoop /etc/condor /test

# Clean up
RUN rm -rf /tmp/* ~/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}


WORKDIR /
ENTRYPOINT ["/bin/bash"]




