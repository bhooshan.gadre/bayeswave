#!/bin/bash

cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))"

# convert README to index.rst for sphinx
pandoc --from=gfm --to=rst --output=index.rst ../README.md

# add a toctree to link the other markdown files
cat >> index.rst << EOF

.. toctree::
   :hidden:

   install
   quickstart
   running
   examples
EOF

# run sphinx
python3 -m sphinx -M html . _build
