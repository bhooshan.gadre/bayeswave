# BayesWaveUtils
Python utilities for bayeswave plot generation and web page creation

Install with:
```
python setup.py install (--prefix=PREFIX)
```

# BayesWavePipe

Utilities for HTCondor workflow generation for the BayesWave gravitational wave
data analysis algorithm

An example configuration file example.ini is included with this package.  You
can set up a demo analysis with e.g.,

```
    bayeswave_pipe example.ini \
    --trigger-time 1187008882.4457 \
    --work-dir example  \
    --sim-data 
```

TIP: it is recommended that you copy the config file elsewhere.  Cluttering up
the source repository is bad.
