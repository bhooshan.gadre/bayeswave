#!/bin/bash

#
# Generate sim-inspiral table using lalapps_inspinj and NR data
#

numrel_data="SXS_BBH_0001_Res5.h5"
catalogfile="SXS_BBH_0001_Res5.xml.gz"

lalapps_make_nr_hdf_catalog \
    -o ${catalogfile} \
    -i ${numrel_data} 


seed=`lalapps_tconvert now`
gpsstart=${seed} 
gpsend=$((${gpsstart}+10))

outfile="HLV-INJECTIONS-NR_`echo ${numrel_data} | sed 's/.h5//g'`"

lalapps_inspinj \
    --seed ${seed} --f-lower 30 --gps-start-time ${gpsstart} \
    --gps-end-time ${gpsend} --waveform NR_hdf5threePointFivePN \
    --amp-order 0 \
    --ninja2-mass --nr-file ${catalogfile} \
    --time-step 10 --time-interval 5 --l-distr random \
    --i-distr uniform \
    --m-distr nrwaves --disable-spin \
    --min-mtotal 100 --max-mtotal 100 \
    --taper-injection start --output ${outfile} \
    --dchirp-distr uniform  --min-distance 500000 --max-distance 500000 
