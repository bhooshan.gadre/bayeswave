#!/bin/bash
set -e

INSTALL_PREFIX=$1

rm -rf build
mkdir -p build
pushd build
cmake3 .. \
	-DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_EXPORT_COMPILE_COMMANDS=true
cmake3 --build . -- VERBOSE=1
cmake3 --build . --target install
popd
