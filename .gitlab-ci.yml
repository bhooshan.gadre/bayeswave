variables:
  BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  COMMIT: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  NIGHTLY: $CI_REGISTRY_IMAGE:nightly
  TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  BUILD_DIR: test-install
  BUILD_TARGET: $CI_PROJECT_DIR/$BUILD_DIR
  TEST_OUTPUT: test-output

stages:
  - build
  - test
  - docker
  - deploy

build-env:
  stage: build
  script:
    - mkdir -p $BUILD_TARGET
    - sed "s|INSTALL_DIR|$BUILD_DIR|g" $CI_PROJECT_DIR/etc/bayeswave-user-env.sh > $BUILD_TARGET/bayeswave-user-env.sh
  artifacts:
    paths:
      - $BUILD_DIR

build-bayeswave:
  stage: build
  image: ligo/software:el7
  script:
    - pushd $CI_PROJECT_DIR
    - mkdir -p build
    - cmake3 . -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=true -DCMAKE_INSTALL_PREFIX=$BUILD_DIR
    - cmake3 --build . -- VERBOSE=1
    - cmake3 --build . --target install
    - popd
  artifacts:
    paths:
      - $BUILD_DIR

build-BayesWaveUtils:
  stage: build
  image: ligo/software:el7
  script: 
    - pushd BayesWaveUtils
    - python setup.py install --prefix $BUILD_TARGET
    - popd
  artifacts:
    paths:
      - $BUILD_DIR

# FIXME: including the full commandline here is illustrative.  We have a test
# script in the repository that does the same thing.
test:BayesWave:
  stage: test
  image: ligo/software:el7
  script:
    - source $BUILD_DIR/bayeswave-user-env.sh
    - cat $BUILD_DIR/bayeswave-user-env.sh
    - ls -R $BUILD_DIR 
    - BayesWave --help
    - "BayesWave --ifo H1 --H1-flow 32 \
      --H1-cache LALSimAdLIGO --H1-channel LALSimAdLIGO \
      --trigtime 900000000.00 --srate 512 --seglen 4 --PSDstart 900000000 \
      --PSDlength 1024 --NCmin 2 --NCmax 2 --dataseed 1234 \
      --Niter 500 --outputDir $TEST_OUTPUT"
  dependencies:
    - build-env
    - build-bayeswave

test:BayesWavePost:
  stage: test
  image: ligo/software:el7
  script:
    - source $BUILD_DIR/bayeswave-user-env.sh
    - BayesWavePost --help
  dependencies:
    - build-env
    - build-bayeswave

test:bayeswave_pipe:
  stage: test
  image: ligo/software:el7
  script:
    - ls $BUILD_DIR
    - source $BUILD_DIR/bayeswave-user-env.sh
    - bayeswave_pipe --help
  dependencies:
    - build-env
    - build-BayesWaveUtils

test:docker:
  stage: test
  image: docker:latest
  before_script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
  script:
    - docker build --pull -t $CI_REGISTRY_IMAGE:latest --file Dockerfile .
    - docker run $CI_REGISTRY_IMAGE:latest /test-bayeswave.sh

# FIXME: Add a proper test stage for the docker images

docker:latest:
  stage: docker
  image: docker:latest
  only:
    - master@lscsoft/bayeswave
    - master@james-clark/bayeswave
  before_script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
  script:
    - docker build --pull -t $CI_REGISTRY_IMAGE:latest --file Dockerfile .
    - docker push $CI_REGISTRY_IMAGE:latest


docker:tags:
  stage: docker
  image: docker:latest
  only:
    - tags@lscsoft/bayeswave
    - tags@james-clark/bayeswave
  before_script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
  script:
    - docker build --pull -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG --file Dockerfile .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

docs:
  stage: build
  image: python:3.7-slim-stretch
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  before_script:
    # install pandoc >= 2.0.0
    - apt-get -yqq update
    - apt-get -yqq install curl
    - curl --location --output pandoc.deb https://github.com/jgm/pandoc/releases/download/2.7.2/pandoc-2.7.2-1-amd64.deb
    - dpkg --install pandoc.deb ||  apt-get -y -f install; dpkg --install pandoc.deb; 
    # install python dependencies
    - python3 -m pip install -r doc/requirements.txt
  script:
    - bash -ex doc/build.sh
  artifacts:
    paths:
      - doc/_build
  cache:
    paths:
      - .cache/pip

pages:
  stage: deploy
  dependencies:
    - docs
  only:
    - tags@lscsoft/bayeswave
  script:
    - mv doc/_build/html public
  artifacts:
    paths:
      - public
